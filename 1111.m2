----------------------------------------------------------------------
-- M2-script accompanying the proof that there exists no plane octic curve with four (or more) [3;3]-points, by excluding a certain configuration of conics. That is, showing that N_{1111} is empty.
--
-- Filename 1111.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
clearAll();
load "octicsFunctions.m2";
--
-- This script excludes the configuration of four conics with four intersection points, where at each intersection point, three of the conics come together, and all three being tangent there.
-- Using plane automorphisms, we can fix three of the points and the conic, hence the tangents at the three points. We then consider a variable fourth point, and (depending on it) the remaining three conics uniquely determined by the condition that they pass through two of the three points in prescribed tangent direction, and through the fourth point (in any direction). If there were a configuration as above, then for some choice of the fourth point, all three tangents at the point would be tangent, but this does not happen, as we show below.
--
-- Notation:
-- A = QQ[s_0,s_1,s_2,u]/s_0*s_1*s_2*u-1 is the affine space of parameters we have to consider,
-- S = A[x,y,z] is the homogeneous coordinate ring of the trivial PP^2 bundle over A, and we consider the points P_0  = (0;0;1), P_1 = (1;0;0), P_2 = (0;1;0) and the variable point Q = (s_0;s_1;s_2). Furthermore, we have the conic defined by xy+xz+yz, passing through P_0, P_1 and P_2 with tangent line T_0 = x + y, T_1 = y + z, and T_2 = x + z, respectively. By construction, Q is off the coordinate axes, which is okay since we want our conics to be irreducible.
--
A = QQ[s_0,s_1,s_2,u]/ideal(s_0*s_1*s_2*u-1);
S = A[x,y,z];
Q = idealFromCoords(s_0,s_1,s_2);
L = {y,z,x};
T = {x+y,y+z,x+z};
P = apply(L,T,(l,t)->ideal(l,t));
PwT = apply(L,T,(l,t)->ideal(l^2,t));
conic = x*y+x*z+y*z;
assert(ideal super basis(2,intersect PwT) == ideal conic);
-- The ideals for the configurations of two of P_0, P_1, P_2 with tangent T_0 and the point Q, to find the conics through those configurations.
groups = { intersect(PwT_0,PwT_1,Q),
	intersect(PwT_0,PwT_2,Q),
	intersect(PwT_1,PwT_2,Q) };
families = apply(groups,i->universalFamily(2,i));
-- In each case, there is only one conic:
assert all(families,i->#(i_2)==1);
conics = apply(families,i->i_3);
-- The conics have an unnecessary parameter, as a by-product of the universalFamily function, which we set to 1 for all of these:
tmp = apply(3,i->map(S,ring (conics_i),gens(S)|{1}));
conics = apply(3,i->(tmp_i)(conics_i));
use S;
-- Getting tangents we want to be multiples of another:
DQconics= apply(conics,
	i->((vars S)*sub(jacobian(ideal i),{x=>s_0,y=>s_1,z=>1}))_(0,0)
	);
m = matrix apply(DQconics,c->apply(gens S,w->c//w));
-- We want m to be of rank 1:
conditions = sub(trim minors(2,m),A);
assert(codim conditions == 1);
reducedConditions = radical conditions;
assert(reducedConditions == ideal sub(conic,{x=>s_0,y=>s_1,z=>s_2}));
--
-- Hence, the three conics fulfill the [3;3]-point-condition at Q only if Q lies on the other conic, which means that all three conics agree.
--
-- EOF 1111.m2 -------------------------------------------------------