----------------------------------------------------------------------
-- M2-script to study octics curves in PP^2 with a quadruple-point and three [3;3]-points, i.e., the stratum N_{1112}.
--
-- Filename 1112.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
-- If an octic has three [3;3]-points and a quadruple-point, then either the three distinguished lines are concurrent with intersection point the quadruple-point and the octic contains the lines (and the residual quintic is rational and irreducible), or the octic is the union of a conic, two lines tangent to the conic, say at P_0 and P_1, and a quartic which is tangent to the conic at P_0 and P_1 as well and which has a tac-node in a third point P_2 on the conic and an ordinary double-point at the intersection of the two lines. We will compute the dimensions of these two components.
--
-- I  Concurrent distinguished tangent lines (N'_{1112}).
--
clearAll();
load "octicsFunctions.m2"
--
-- The configuration of three concurrent lines and three non-collinear points on those lines is fixed by finitely many automorphisms and we can, without loss of generality, fix those four points.
--
-- Notation:
-- S = QQ[a_0,a_1][x,y,z] is the homogeneous coordinate ring of the trivial PP^2-family over rational AA^2. 
-- We will consider all octic forms with [3;3]-points in the points P_0 = (0;0;1), P_1 = (1;0;0) and P_2 = (0;1;0) and a quadruple-point in P_3 = (1;1;1), where the distinguished tangent at P_i is the line joining P_i and P_3. (There is a pencil of such octics, which we will parametrise using the extra parameters a_0, a_1.)
--
use QQ[x,y,z];
P = {idealFromCoords(0,0,1), idealFromCoords(1,0,0),
     idealFromCoords(0,1,0), idealFromCoords(1,1,1)};
L = {y,z,z};
T = {x-y,y-z,x-z};
F = {z,x,y}; -- aux. localisation variables
scan(3,i-> assert( P_i == ideal(L_i,T_i) ) );
P33 = apply(3,i->get33Ideal(L_i,T_i,F_i));
-- The lines are chosen as explained above:
assert all(3, i->(T_i)%intersect(P_i,P_3) == 0);
-- The ideal containing the constraints:
I = intersect(intersect(P33),P_3^4);
-- the universal family:
(S,inc,params,f) = universalFamily(8,I);
assert(#params == 2);
use S;
T = apply(T,i->inc(i));
-- Sanity check: Every octic of the family contains all tree distinguished tangent lines:
assert( f%(T_0*T_1*T_2) == 0 );
-- It remains to show that the generic element has ordinary [3;3]-points at P_0, P_1 and P_2, an ordinary quadruple-point at P_3, and is singular only in those four points.
use S;
badParams = degeneracyConditions33Point(f,T_0,x,y,z);
badParams = badParams + degeneracyConditions33Point(f,T_1,z,y,x);
badParams = badParams + degeneracyConditions33Point(f,T_2,z,x,y);
-- Translate to move quadruple-point to (0;0;1):
g = sub(f,{x=>x+z,y=>y+z,z=>z});
badParams = badParams + degeneracyConditionsQuadruplePoint(g,x,y,z);
assert(codim badParams == 1);
-- Outside the bad locus, the singular locus is as expected:
g = (radical badParams)_*;
Sgood = S[u]/(u*product(g)-1);
J =  sub(radical(ideal(jacobian ideal f)+f),Sgood);
assert(J == sub(intersect P,Sgood));
-- As claimed.
--
-- Thus, the pencil given by f generically defines admissible octics and so the dimension of this component is one.
--
-- A note aside for the record: The (reduced) bad locus is given by the three points (1;0),(2;-1),(1;-1) (in PP^1 = Proj QQ[t_0,t_1]) and the octics we obtain for these parameters are the union of twice the conic through P_0, P_1 and P_2 tangent to two of them, the two corresponding distinguished lines, and twice the remaining distinguished line (--> members of M^3 with 2T_{2,3,infty} +  T_{2,4,infty} + 2T_{2,infty,infty}).
--
-- II  Distinguished tangents along a conic (N'_{1112}).
--
clearAll();
-- Requires:
load "octicsFunctions.m2";
--
-- The configuration is determined by two lines, each one of them with a point (not the intersection point), and a third point in general position. The automorphism group of the plane acts transitively on those configurations, with finite stabilisers. Therefore, we may fix any. The remaining data are determined by this configuration: The (to become quadruple-)point is the intersection of the lines and there is a unique conic through the other three points which is tangent to the lines, thus determining the third distinguished tangent direction.
--
-- Notation as in I
--
use QQ[x,y,z];
P = {idealFromCoords(0,0,1),idealFromCoords(1,0,0),
     idealFromCoords(0,1,0),idealFromCoords(1,1,1)};
L = {y,z,z};
T = {x-y,y-z,x+z};
F = {z,x,y}; -- aux. localisation variables
P33 = apply(3,i->get33Ideal(L_i,T_i,F_i));
PwT = apply(3,i->ideal(L_i^2,T_i));
conic = x*y-x*z+y*z;
scan(3,i->assert(P_i == ideal(L_i,T_i)));
assert(ideal super basis(2,intersect(PwT)) == ideal conic);
assert(ideal(T_0,T_1) == P_3);
I = intersect(intersect(P33),P_3^4);
(S,inc,params,f) = universalFamily(8,I);
assert(#params == 2);
T = apply(T,i->inc(i));
conic = inc(conic);
use S;
-- Sanity check: Every octic of the family contains the two distinguished tangents at P_0 and P_1 and the conic:
assert(f%(T_0*T_1*conic) == 0);
-- The generic octic of the system has non-degenerate [3;3]-points at P_0, P_1 and P_2 and a non-degenerate quadruple-point at P_3 and no other singularities:
badParams = degeneracyConditions33Point(f,T_0,x,y,z);
badParams = badParams + degeneracyConditions33Point(f,T_1,z,y,x);
badParams = badParams + degeneracyConditions33Point(f,T_2,z,x,y);
-- Translate to move quadruple-point to (0;0;1):
g = sub(f,{x=>x+z,y=>y+z,z=>z});
badParams = badParams + degeneracyConditionsQuadruplePoint(g,x,y,z);
assert(codim badParams == 1);
-- Outside the bad locus, the singular locus is as expected:
g = (radical badParams)_*;
Sgood = S[u]/(u*product(g)-1);
J = radical sub((ideal(jacobian ideal f)+f),Sgood);
assert(J == sub(intersect P,Sgood));
-- As claimed.
--
-- Thus, the pencil given by f generically defines admissible octics and so the dimension of this component is 1.
--
-- A note aside for the record: The (reduced) bad locus is given by the three points (1;0),(2;-1),(1;-1) (in PP^1 = Proj QQ[a_0,a_1]). (This is not a copy-paste mistake, the points are really the same in both cases.)
-- The degenerate conics are given by:
--    1. The two lines, the conic, and twice a conic through all four
--       points, tangent at P_2 (--> member of M^2 with T_{2,3,infty}
--       + T_{2,4,infty} + 2T_{2,6,infty});
--    2. The conic once and all three lines twice (--> member of M^3
--       with 3T_{2,3,infty}+3T_{2,infty,infty});
--    3. Both lines (once each), the conic twice and the line joining
--       P_2 and P_3 twice (--> member of M^3 with T_{2,3,infty} + 
--       T_{2,4,infty} + T_{2,infty,infty}).
-- Observe that 3. is exactly the kind of degeneration that occurred in II above. Thus, in the closures of the components in the full moduli space meet in the stratum M^(2).
--
-- EOF 1112.m2 -------------------------------------------------------