This repository contains the Macaulay2-scripts proving things like irreducibility and computes the dimension of the components of the strata of a certain stratification of the moduli space plane curves of degree 8 with global log-canonical threshold at least 1/2.

For more about the maths, see Ben Anthes' thesis Gorenstein stable surfaces satisfying K_X^2 = 2 and χ(O_X)=4, Marburg 2018, https://doi.org/10.17192/z2019.0050.

The file octicsFunctions.m2 provides the functions used in the main scripts and running main.m2 means running all scripts, one after the other.

The scripts were written by Ben Anthes and they are inspired very much by a similar script by Sönke Rollenske.

More information about Macaulay2 (Grayson, Stillman et al.) can be found at http://www.math.uiuc.edu/Macaulay2/