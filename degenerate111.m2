----------------------------------------------------------------------
-- M2-script to study octics in PP^2 with three [3;3]-points, at least one degenerate, i.e., the strata N_{111bar}, N_{11bar^2} and N_{1bar^3}
--
-- Filename degenerate111.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
clearAll();
load "octicsFunctions.m2";
--
-- We will have to run through this twice, since the two different configurations, the special case with tangents along a conic and the general case, work better on different base rings.
--
-- Notation:
-- A0 = QQ[s_0,s_1,s_2,t_2] is the affine coordinate ring of AA^4 over the rational numbers,
-- S0 = A0[x,y,z] is the homogeneous coordinate ring of the projective plane over AA^4 and we consider the points P_0 = (0;0;1), P_1 = (1;0;0) and P_2 = (0;1;0) on the distinguished lines {y = -x}, {z = -y} and {x = t_2 z}, respectively.
--
-- The common setup:
A0 = QQ[s_0,s_1,s_2,t_2];
S0 = A0[x,y,z];
L = {x,y,z};
T = {y+L_0,z+L_1,x-t_2*L_2};
F = {z,x,y};
P = apply(L,T,pair->trim ideal pair);
P33 = apply(3,i->get33Ideal(L_i,T_i,F_i));
PwT = apply(T,P,(l,p)->trim(p^2+l));
conic = super basis(2,intersect(PwT_0,PwT_1,P_2));
assert(conic == matrix(x*y+x*z+y*z));
conic = conic_(0,0);
assert(conic%PwT_2 == (t_2+1)*y*z);
-- Thus, the special case is t_2 = -1.
--
-- I  The general case t_2 != -1.
-- We also assume t_2 != 0, which is valid since it corresponds to the case where the distinguished tangent points towards another [3;3]-point, but then any octic contains the line twice.
A = A0[r,Degrees=>{ -1 }]/((t_2+1)*t_2*r-1);
locA = map(A,A0);
use S0;
--
-- Preparation, generating the universal family with three [3;3]-points in general position and the degeneracy conditions:
(RS,inc,params,f) = universalFamily(8,intersect P33);
assert(#params == 9);
M0 = apply(3,i->
  directedDegConditions33Point(f,inc L_i, inc T_i, inc F_i, s_i));
M = apply(M0,m->locA m);
--
-- I.1  One of the [3;3]-points degenerate (N'_{111bar})
--
assert(isSurjective M_0 and numgens target M_0 == 2);
-- Hence, the stratum is irreducible. The kernel of M_0 has dimension  9 - 2 = 7; thus, the stratum is of dimension 7 + 2 - 1 = 8. (+2 from t_2 and s_0, -1 from rescaling.) It is easy to see that this stratum is inhabited, but it also follows from I.4 below.
--
-- I.2  Two of the [3;3]-points degenerate (N'_{11bar^2})
--
N = M_0 || M_1;
assert(isSurjective N and numgens target N == 4);
-- Thus, the stratum is irreducible and of dimension 5 + 3 - 1 = 7, since the kernel dimension is 9 - 4 = 5. It is non-empty by I.4.
--
-- I.3  Three degenerate [3;3]-points (N'_{1bar^3})
-- Sanity check: Since we have excluded t_2 = 0, -1, M_2 is surjective
assert isSurjective(M_2);
--
N = M_0 || M_1 || M_2;
-- The rank of N drops for one set of parameters:
--droppingRankCond = minors(numgens target N,N);
--droppingRankCond = ideal gens gb droppingRankCond;
-- took more than a day. Therefore, hard-coded:
droppingRankCond = ideal(
    s_0-(-t_2-19)/2,  s_1-(t_2-1)/2,  s_2-(19*t_2+1)/2,
    t_2^2+18*t_2+1,  16*r+17*t_2+305);
-- Note: t_2^2+18*t_2+1 = (t_2-(4*sqrt(5)-9))*(t_2-(-4*sqrt(5)-9)); moreover, the last generator is superfluous.
-- We investigate what happens for those special parameters.
B = A/droppingRankCond;
pr = map(B,A);
N' = pr N;
-- Since mingens fails to find the minimal number of generators even though B is a field, we use basis, which is ok since B is a field:
K' = super basis kernel N';
BS = B[gens S0];
BRS = BS[params];
f' = sub(f,BRS);
I = sub(idealFromImage(K',f',gens BRS),BS);
-- It turns out that all members of I contain a certain quintic:
quintic = 8*x^3*y^2+(t_2+21)*x^2*y^3+16*x^3*y*z+(-11*t_2+33)*x^2*y^2*z+(-6*t_2+2)*x*y^3*z+8*x^3*z^2+(-33*t_2+11)*x^2*y*z^2-88*t_2*x*y^2*z^2+(-55*t_2-3)*y^3*z^2+(-21*t_2-1)*x^2*z^3+(-42*t_2-2)*x*y*z^3+(-21*t_2-1)*y^2*z^3,8*t_2*x^3*y^2+(3*t_2-1)*x^2*y^3+16*t_2*x^3*y*z+(231*t_2+11)*x^2*y^2*z+(110*t_2+6)*x*y^3*z+8*t_2*x^3*z^2+(605*t_2+33)*x^2*y*z^2+(1584*t_2+88)*x*y^2*z^2+(987*t_2+55)*y^3*z^2+(377*t_2+21)*x^2*z^3+(754*t_2+42)*x*y*z^3+(377*t_2+21)*y^2*z^3;
assert((gens I)%quintic == 0);
-- I claim that this quintic has singularities of type A_4 at the point P_0, P_1 and P_2. Thus, the union with a general cubic passing through the P_i in prescribed tangent direction is a singularity of type J_{2,1}. Since there is a four-dimensional space of such cubics, this is what is going on geometrically.
-- Since the base field B is not QQ, we have to do this by hand.
D = B[u,v];-- The affine plane over B
sigma = map(D,D,{u,u*v});-- Blowing up the origin
-- At P_0:
l = map(D,BS,{u,v,1});-- A naive localisation away from {z = 0}
-- The distinguished tangent line localised:
tgt = (sigma l sub(T_0,BS))//u;
-- Obtaining the strict transform of the quintic:
totalTrans = sigma l(quintic);
strictTrans = totalTrans//u^2;-- Indeed:
assert(strictTrans*u^2 == totalTrans);
-- And it has an infinitely near double-point:
assert(ideal sub(strictTrans,{u=>0}) == ideal(tgt^2));
-- Moving the double-point to the origin:
tmp = sub(strictTrans,{v=>v-sub(tgt,{v=>0})});
assert(tmp%(ideal(u,v))^2 == 0);
totalTrans' = sigma tmp;
strictTrans' = totalTrans'//u^2;-- Indeed:
assert(strictTrans'*u^2 == totalTrans');
sndTgt = v-l(s_0);
assert(ideal sub(strictTrans',{u=>0}) == ideal(sndTgt^2));
-- However, the strict transform is smooth and tangent along the exceptional line:
sing = ideal(jacobian matrix strictTrans')+ideal(strictTrans');
assert(sing == ideal(1_D));
assert(strictTrans'%ideal(u,sndTgt^2) == 0);
--
-- Now the same at P_1:
l = map(D,BS,{1,u,v});-- A naive localisation away from {x = 0}
-- The distinguished tangent line localised:
tgt = (sigma l sub(T_1,BS))//u;
-- Obtaining the strict transform of the quintic:
totalTrans = sigma l(quintic);
strictTrans = totalTrans//u^2;-- Indeed:
assert(strictTrans*u^2 == totalTrans);
-- And it has an infinitely near double-point:
assert(ideal sub(strictTrans,{u=>0}) == ideal(tgt^2));
-- Moving the double-point to the origin:
tmp = sub(strictTrans,{v=>v-sub(tgt,{v=>0})});
assert(tmp%(ideal(u,v))^2 == 0);
totalTrans' = sigma tmp;
strictTrans' = totalTrans'//u^2;-- Indeed:
assert(strictTrans'*u^2 == totalTrans');
sndTgt = v-l(s_1);
assert(ideal sub(strictTrans',{u=>0}) == ideal(sndTgt^2));
-- However, the strict transform is smooth and tangent along the exceptional line:
sing = ideal(jacobian matrix strictTrans')+ideal(strictTrans');
assert(sing == ideal(1_D));
assert(strictTrans'%ideal(u,sndTgt^2) == 0);
--
-- And a last time, at P_2:
l = map(D,BS,{v,1,u});-- A naive localisation away from {y = 0}
-- The distinguished tangent line localised:
tgt = (sigma l sub(T_2,BS))//u;
-- Obtaining the strict transform of the quintic:
totalTrans = sigma l(quintic);
strictTrans = totalTrans//u^2;-- Indeed:
assert(strictTrans*u^2 == totalTrans);
-- And it has an infinitely near double-point:
assert(ideal sub(strictTrans,{u=>0}) == ideal(tgt^2));
-- Moving the double-point to the origin:
tmp = sub(strictTrans,{v=>v-sub(tgt,{v=>0})});
assert(tmp%(ideal(u,v))^2 == 0);
totalTrans' = sigma tmp;
strictTrans' = totalTrans'//u^2;-- Indeed:
assert(strictTrans'*u^2 == totalTrans');
sndTgt = v-l(s_2);
assert(ideal sub(strictTrans',{u=>0}) == ideal(sndTgt^2));
-- However, the strict transform is smooth and tangent along the exceptional line:
sing = ideal(jacobian matrix strictTrans')+ideal(strictTrans');
assert(sing == ideal(1_D));
assert(strictTrans'%ideal(u,sndTgt^2) == 0);
-- As claimed.
--
-- This means that we could have extra components where the degeneracy conditions are satisfied. But below we show that all elements of these potential extra components are limits of elements of the main component. Thus, there is really just one component.
--
-- We have to investigate which of the curves parametrised by the kernel of N' are degenerations of curves parametrised by the kernel of N. For this, we consider the four 1-parameter degenerations where three of the four parameters t_2, s_0, s_1, s_2 are fixed.
use A;
conditions = (droppingRankCond_*)_{0..3};
parameterIdeals = apply(4,i->sub(ideal(drop(conditions,{i,i})),A));
rings = apply(parameterIdeals,quotient);
mapsRA = apply(rings,R->map(R,A));
mapsBR = apply(rings,R->map(B,R));
assert all(mapsBR,mapsRA,m->m_0 * m_1 === pr);-- (sanity check)
matrices = apply(mapsRA,m->m N);
kernels = apply(matrices,kernel);
limits = apply(kernels,mapsBR,(i,p)->p(i));
degenerations = sum limits;
assert(degenerations == kernel N');
-- Thus, the potential extra components are actually contained in the main component, which is, therefore, still irreducible.
--
-- We compute the dimension of N'_{1bar^3} as 3 + 4 - 1 = 6; the locus corresponding to the special parameters is of dimension 4 - 1 = 3.
--
-- I.4  The strata are not empty
-- It follows from our line of arguments so show that N'_{1bar^3} is inhabited, since appropriate degenerations will become members of the other strata. In fact, we have seen in I.3 that this component is indeed non-empty, but to see that the dimension is really 6 (as opposed to 3), we also need a curve for parameters where N has maximal rank, e.g., t_2 = 2 and s_0 = s_1 s_2 = -1.
-- The following commented lines show how the hard-coded element fRand was obtained:
--B = symbol B;
--B = toField(A/(t_2-2,s_0+1,s_1+1,s_2+1));
--prB = map(B,A);
--N' = prB N;
--BS = B[gens S0];
--BRS = BS[params];
--f' = sub(f,BRS);
--I = sub(idealFromKernel(N',f',gens BRS),BS);
--fRand = randomElement(I);
-- This resulted in the following:
BS = QQ[gens S0];
specialise = map(BS, S0, gens(BS) | {-1,-1,-1,2});
use BS;
fRand = (3942/7)*x^5*y^3-(2336/7)*x^4*y^4+2628*x^3*y^5+(11826/7)*x^5*y^2*z+(6679/21)*x^4*y^3*z+(21884/21)*x^3*y^4*z-15768*x^2*y^5*z+(11826/7)*x^5*y*z^2-(22663/14)*x^4*y^2*z^2-(97338/7)*x^3*y^3*z^2-(16846/7)*x^2*y^4*z^2+31536*x*y^5*z^2+(3942/7)*x^5*z^3-(38686/7)*x^4*y*z^3-(63821/7)*x^3*y^2*z^3+(180575/7)*x^2*y^3*z^3+7800*x*y^4*z^3-21024*y^5*z^3-(136753/42)*x^4*z^4+(77075/21)*x^3*y*z^4+(16275/2)*x^2*y^2*z^4-(162956/21)*x*y^3*z^4-(188392/21)*y^4*z^4+468*x^3*z^5+1404*x^2*y*z^5+1404*x*y^2*z^5+468*y^3*z^5;
-- First of all, the singular locus is supported at P_0, P_1 and P_2:
expected = intersect apply(P,i->specialise i);
sing = ideal( jacobian matrix fRand )+fRand;
assert(radical sing == expected);
-- Furthermore, at all three points, we have singularites of type J_{2,1}, i.e., the mildest form of degenerate [3;3]-points.
assert(isJ21(fRand, specialise T_0, x, y, z));
assert(isJ21(fRand, specialise T_1, y, z, x));
assert(isJ21(fRand, specialise T_2, x, z, y));
-- This concludes the discussion about the case of general distinguished tangent directions.
--
-- II  The special case t_2 = -1.
-- That is, all three tangents are along the conic. This conic is then contained in the octic and the residual septic meets it in three points of multiplicity at least four, but 2*6 = 12 = 3*4, so that the intersection multiplicity has to be exactly four at all three points, assuming the octic is reduced. But if s_0, or s_1 or s_2 equals 1, then the intersection multiplicity is at least 5 at some point and the octic is not admissible. Therefore, we can exclude these values right from the start.
use A0;
A = A0[r,Degrees=>{ -1 }]/(r*(s_0-1)*(s_1-1)*(s_2-1)+1,t_2+1);
prA = map(A,A0);
S = A[gens S0];
prS = map(S,S0);
--
-- Preparation, generating the universal family with three [3;3]-points in special position and the degeneracy conditions:
I = intersect apply(P33,i-> prS i);
(RS,inc,params,f) = universalFamily(8, I);
assert(#params == 10);
M = apply(3,i->directedDegConditions33Point(f,
	inc prS L_i, inc prS T_i, inc prS F_i, s_i));
assert all(M,m->numgens target m == 2);
assert(
    isSurjective(M_0) and
    isSurjective(M_0 || M_1) and
    isSurjective(M_0 || M_1 || M_2)
);
-- Therefore, the conditions, two for each point, are independent and we get linear spaces of dimension 10 - 2 = 8, 10 - 4 = 6 and 10 - 6 = 4, respectively. Since the stabiliser group of the configuration in the plane is finite, but s_0, s_1 and s_2 contribute one dimension each, the corresponding strata of the configuration are irreducible and of dimension 8 + 1 - 1 = 8, 6 + 2 - 1 = 7 and 4 + 3 - 1 = 6, respectively.
--
-- As in I.4, once we have constructed an inhabitant for the smallest stratum, it follows that each stratum is inhabited. Specialising now means choosing values for s_0, s_1 and s_2, different from 1, say s_0 = s_1 = s_2 = -1. Then r = 1/8.
BS = QQ[gens S0];
specialise = map(BS, S, gens(BS) | {1/8,-1,-1,-1,-1});
-- Again, we hard-coded an octic form obtained 'randomly':
--I = sub(idealFromKernel(M_0 || M_1 || M_2,f,params),S);
--I' = specialise I;
--fRand = randomElement(I');
-- This resulted in:
fRand = -(59/3)*x^5*y^3-(1/3)*x^4*y^4-(59/3)*x^3*y^5-59*x^5*y^2*z-(278/9)*x^4*y^3*z+9*x^3*y^4*z-59*x^2*y^5*z-59*x^5*y*z^2-(331/3)*x^4*y^2*z^2-(8/3)*x^3*y^3*z^2+(28/3)*x^2*y^4*z^2-59*x*y^5*z^2-(59/3)*x^5*z^3-(388/3)*x^4*y*z^3-(451/3)*x^3*y^2*z^3-(92/3)*x^2*y^3*z^3-(29/3)*x*y^4*z^3-(59/3)*y^5*z^3-(446/9)*x^4*z^4-(416/3)*x^3*y*z^4-(415/3)*x^2*y^2*z^4-(530/9)*x*y^3*z^4-(29/3)*y^4*z^4-(59/3)*x^3*z^5-59*x^2*y*z^5-59*x*y^2*z^5-(59/3)*y^3*z^5;
-- First of all, the singular locus is supported at P_0, P_1 and P_2:
expected = intersect apply(P,i->specialise prS i);
sing = ideal( jacobian matrix fRand )+fRand;
assert(radical sing == expected);
-- Furthermore, at all three points, we have singularities of type J_{2,1}, i.e., the mildest degenerate [3;3]-points possible:
assert(isJ21(fRand, specialise prS T_0, x, y, z));
assert(isJ21(fRand, specialise prS T_1, y, z, x));
assert(isJ21(fRand, specialise prS T_2, x, z, y));
-- As claimed.
--
-- EOF degenerate111.m2 ----------------------------------------------