----------------------------------------------------------------------
-- M2-script to study octics in PP^2 with two [3;3]-points and one quadruple-point, at least one degenerate, i.e., the strata N_{112bar}, N_{11bar2}, N_{11bar2bar}, N_{1bar1bar2} and N_{1bar1bar2bar}.
--
-- Filename degenerate112.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
clearAll();
load "octicsFunctions.m2";
--
-- Notation:
-- A0 is the affine coordinate ring of AA^5 with coordinates s_0, s_1, t_0, t_1 and t_2,
-- S0 = A0[x,y,z] is the homogeneous coordinate ring of the projective plane over AA^5. We consider three points with distinguished lines:
-- P_0 = (0;0;1), T_0 = y - t_0 x
-- P_1 = (1;0;0), T_1 = z - t_1 y
-- P_2 = (1;1;1), T_3 = y - x - t_3 (z - x)
-- We will consider quadruple-points and [3;3]-points, both ordinary and degenerate, supported at those points and with distinguished tangent direction spanned by their distinguished lines as indicated above. The variables s_0 and s_1 will be there to parametrise the higher degeneracy directions of the [3;3]-points.
--
-- Recall that there are three disjoint components in N_{112} which correspond to the cases that none, one ot both distinguished tangent directions at the [3;3]-points points towards the quadruple-point. We will run through this three times since each case requires a different base ring.
--
-- The common setup:
A0 = QQ[s_0,s_1,t_0,t_1,t_2];
S0 = A0[x,y,z];
L = {x,z,z-x};
T = {y-t_0*L_0,y-t_1*L_1,y-x-t_2*L_2};
F = {z,x,z};
P = apply(L,T,pair->trim ideal pair);
P33 = apply(2,i->get33Ideal(L_i,T_i,F_i));
-- Q = getDegenerateQuadrupleIdeal(L_2,T_2,F_2);
--
-- I  Distinguished tangents in general direction (t_0, t_1 != 1).
-- If we let t_2 vary (if necessary), we have enough automorphisms left to fix t_0 and t_1 to, say, t_0 = t_1 = -1.
A = A0/(t_0+1,t_1+1);
pr = map(A,A0);
S = A[gens S0];
prS = map(S,S0);
I = intersect(prS P33_0, prS P33_1, prS P_2^4);
(RS,inc,params,f) = universalFamily(8,I);
assert(#params == 11);
--
M0 = directedDegConditions33Point(f,
    inc prS L_0, inc prS T_0, inc prS F_0, s_0);
M1 = directedDegConditions33Point(f,
    inc prS L_1, inc prS T_1, inc prS F_1, s_1);
M2 = tangencyCond(f,
    inc prS L_2,inc prS T_2,inc prS F_2,
    Multiplicity=>4, Degree=>2);
-- I.1  One of the points degenerate
-- Imposing degeneracy at P_i means taking the linear system defined by the kernel of Mi.
assert(numgens target M0 == 2 and isSurjective M0);-- N'_{11bar2}
assert(numgens target M1 == 2 and isSurjective M1);-- N'_{11bar2}
assert(numgens target M2 == 2 and isSurjective M2);-- N'_{112bar}
-- Since the rank drops nowhere, both components, N'_{11bar2} and N'_{112bar}, are irreducible and of dimension 11 - 2 + 1 - 1 = 9. Here, 2 is the rank of the matrices, there is 1 extra parameter in each case (s_0, s_1, t_2) and we subtract 1 for rescaling.
--
-- I.2  Two of the points degenerate
-- Now we have to consider the common kernels of pairs of the matrices
M01 = M0 || M1;-- N'_{1bar1bar2}
M02 = M0 || M2;-- N'_{11bar2bar}
-- They are not surjective everywhere; where and why?
-- I.2.1 Both [3;3]-points degenerate
droppingRankCond = minors(4,mingens image M01);
assert(radical droppingRankCond == ideal(s_1-4,s_0-4));
-- Thus, the rank of M01 drops for s_0 = s_1 = 4 and we have to investigate what happens there. This case is neglectable since every octic corresponding to those parameters contains a special conic twice. In fact, it's the unique conic passing through the three points in prescribed tangent direction at P_0 and P_1 and this follows from intersection-theory, but we can also see it here:
M01special = sub(M01,{s_0=>4,s_1=>4});
Jspecial = sub(idealFromKernel(M01special,
	sub(f,{s_0=>4,s_1=>4}), params), S);
specialConic = sub(x*y-3*y^2+x*z+y*z,S);
assert((gens Jspecial)%specialConic^2 == 0);
-- For all other parameters, N has maximal rank 4, so that the component N'_{1bar1bar2} (non-empty by I.4 below) is irreducible of dimension 11 - 4 + 2 - 1 = 8.
--
-- I.2.2 A [3;3]-point and a quadruple-point degenerate
droppingRankCond = minors(4,mingens image M02);
assert(radical droppingRankCond == ideal(2*t_2-1,s_0-4));
-- Thus, the rank of M02 drops for s_0 = 4, t_1 = 1/2. This case is neglectable as well since as in I.2.1, every octic satisfying the constraints for to these parameters contains the very same special conic twice:
M02special = sub(M02,{s_0=>4,t_2=>1/2});
Jspecial = sub(idealFromKernel(M02special,
	sub(f,{s_0=>4,t_2=>1/2}), params), S);
assert((gens Jspecial)%specialConic^2 == 0);
-- As in I.2.1 we conclude that N'_{11bar2bar} is irreducible and 8-dimensional; that it is not empty will be part of I.4 below.
--
-- I.3  All three singularities degenerate
-- We have to look at the common kernel of all three matrices.
N = M0 || M1 || M2;
-- Again, N is not surjective, but the parameters where it is not are irrelevant for us:
droppingRankCond = minors(6,mingens image N);
assert(radical droppingRankCond == intersect(
	ideal(s_1-4,s_0-4),
	ideal(2*t_2-1,s_0-4),
	ideal(2*t_2-1,s_1-4)));
-- These are the cases we have seen in I.2 (up to symmetry), where we already know that there is a conic contained twice. We conclude that the corresponding component is irreducible and has dimension 11 - 6 + 3 - 1 = 7, once we have seen that it is not empty.
--
-- I.4  Non-emptiness of the strata
-- By our line of arguments, it suffices to produce an inhabitant of N'_{1bar1bar2bar} since appropriate deformations of this member will be elements of the other strata.
--
-- For this, we fix more or less arbitrary values for the parameters.
special = map(QQ, A, {1,1,-1,-1,1});
S' = QQ[gens S];
specialS = map(S', S, gens(S') | {1,1,-1,-1,1});
-- We 'randomly' obtained the following octic form for parameters s_0 = s_1 = t_2 = 1:
--J = specialS sub(idealFromKernel(N,f,params),S);
--fRand = randomElement J;
use S';
fRand = -10782*x^5*y^3-14169600*x^4*y^4+(29881520009/210)*x^3*y^5-(7179779171/14)*x^2*y^6+(53866091961/70)*x*y^7-(26927310393/70)*y^8-32346*x^5*y^2*z-(141546708/5)*x^4*y^3*z+(23847941291/210)*x^3*y^4*z+(12022783447/70)*x^2*y^5*z-(10269121787/10)*x*y^6*z+(53856117729/70)*y^7*z-32346*x^5*y*z^2+(688392/5)*x^4*y^2*z^2-(7192780709/42)*x^3*y^3*z^2+(35916966831/70)*x^2*y^4*z^2+(12028972583/70)*x*y^5*z^2-(7176955691/14)*y^6*z^2-10782*x^5*z^3+(142624908/5)*x^4*y*z^3-(23935325167/210)*x^3*y^2*z^3-(2398067559/14)*x^2*y^3*z^3+(1587637157/14)*x*y^4*z^3+(29857735217/210)*y^5*z^3+(71237808/5)*x^4*z^4+(2995907368/105)*x^3*y*z^4+(931964/5)*x^2*y^2*z^4-(988201248/35)*x*y^3*z^4-(1484261588/105)*y^4*z^4-(1592462/105)*x^3*z^5-(1592462/35)*x^2*y*z^5-(1592462/35)*x*y^2*z^5-(1592462/105)*y^3*z^5;
-- First of all, it is singular only at P_0, P_1 and P_2:
expected = intersect apply(P,i->specialS prS i);
sing = ideal(jacobian matrix fRand)+fRand;
assert(radical sing == expected);
-- On the other hand, the singularities there are as desired:
assert isJ21(fRand, specialS prS T_0, x, y, z);
assert isJ21(fRand, specialS prS T_1, z, y, x);
-- Moving P_2 = (1;1;1) to (0;0;1):
fRand' = sub(fRand, {x=>x+z,y=>y+z});
assert isX10(fRand', y, x);
-- Therefore, fRand indeed defines a member of N'_{1bar1bar2bar}. This finishes the discussion of the general configuration.
--
-- II  One tangent general, the other towards the quadruple-point
--     (t_0 = 1, t_1 != 1)
-- We have enough automorphisms left to fix t_1 = -1.
use A0;
A = A0/(t_0-1,t_1+1);
pr = map(A,A0);
S = A[gens S0];
prS = map(S,S0);
I = intersect(prS P33_0, prS P33_1, prS P_2^4);
(RS,inc,params,f) = universalFamily(8,I);
assert(#params == 12);
--
-- II.1  One of the points degenerate
-- This time, when imposing the degeneracy at a [3;3]-point, we have to distinguish whether we impose it at the [3;3]-point whose distinguished tangent points towards the quadruple-point (P_0) or the other one (P_1). The components are denoted by N''a_{11bar2} (if the degeneracy is at P_1) and N''b_{11bar2} (if the degeneracy is at P_0).
M0 = directedDegConditions33Point(f,
    inc prS L_0, inc prS T_0, inc prS F_0, s_0);
M1 = directedDegConditions33Point(f,
    inc prS L_1, inc prS T_1, inc prS F_1, s_1);
M2 = tangencyCond(f,
    inc prS L_2,inc prS T_2,inc prS F_2,
    Multiplicity=>4, Degree=>2);
-- Imposing degeneracy at P_i means taking the linear system defined by the kernel of Mi.
assert(numgens target M0 == 2);-- N''b_{11bar2}
assert(numgens target M1 == 2 and isSurjective M1);-- N''a_{11bar2}
assert(numgens target M2 == 2);-- N''_{112bar}
-- Since the rank of M1 drops nowhere, N''a_{11bar2} is irreducible and of dimension 12 - 2 + 1 - 2 = 9. Here, 2 is the rank of the matrices, there is 1 extra parameter in each case (s_0, s_1, t_2) and we subtract 2 for the stabiliser.
--
-- What happens at P_0 and P_2?
-- II.1.1 N''b_{11bar2}
droppingRankCond = minors(2, mingens image M0);
assert(radical droppingRankCond == ideal(s_0));
-- That is, the rank drops if and only if the second order direction of the degenerate [3;3]-point pointing towards the quadruple-point is trivial. In this case, the line has to be contained twice:
J = sub(idealFromKernel(sub(M0,{s_0=>0}),sub(f,{s_0=>0}),params),S);
assert((gens J)%(prS T_0)^2 == 0);
-- Thus, this case is neglected in the stratum anyways. It follows that the component N''b_{1bar22} is irreducible and 9-dimensional as well.
--
-- II.1.2 N''_{112bar}
droppingRankCond = minors(2, mingens image M2);
assert(radical droppingRankCond == ideal(t_2));
-- That is, the rank drops if and only if the quadruple-point's special tangent direction points towards a [3;3]-point, which as we know is impossible on a reduced octic. Thus, this case is irrelevant as well and so N''_{112bar} is irreducible and has dimension 9.
--
-- That these components are indeed not empty follows from II.4 below.
--
-- II.2  Two of the points degenerate
-- Again, we have two components N''a_{11bar2bar} and N''b_{11bar2bar} defined analogously as in II.1 above.
M01 = M0 || M1;-- N''_{1bar1bar2}
M02 = M0 || M2;-- N''b_{11bar2bar}
M12 = M1 || M2;-- N''a_{11bar2bar}
-- None of them is surjective everywhere.
--
-- II.2.1 N''_{1bar1bar2}
droppingRankCond = minors(4, mingens image M01);
assert(radical droppingRankCond == ideal(s_0));
-- For the same reason as in II.1.1, the corresponding parameters are neglected; thus, N''_{1bar1bar2} is irreducible and of dimension 12 - 4 + 2 - 2 = 8.
--
-- II.2.2 N''b_{11bar2bar}
droppingRankCond = minors(4, mingens image M02);
assert(radical droppingRankCond == ideal(s_0*t_2));
-- The rank of M02 drops if s_0 = 0 or t_2 = 0; the former case is irrelevant by II.1.1 and the latter case is irrelevant by II.1.2. Therefore, N''b_{1bar12bar} is irreducible and 8-dimensional.
--
-- II.2.3 N''a_{11bar2bar}
droppingRankCond = minors(4, mingens image M12);
assert(radical droppingRankCond == ideal(t_2));
-- The rank of M12 drops if t_2 = 0, which is is irrelevant by II.1.2. Therefore, N''a_{1bar12bar} is irreducible and 8-dimensional, too.
--
-- That these components are indeed not empty follows from II.4 below.
--
-- II.3  All three singularities degenerate
N = M0 || M1 || M2;
droppingRankCond = minors(6, mingens image N);
assert(radical droppingRankCond == ideal(s_0*t_2));
-- Since the common rank of all three matrices drops only for s_0 = 0
-- or t_2 = 0, which is irrelevant (cf. II.2.2), we conclude that (if inhabited, see II.4 below), the component N''_{1bar1bar2bar} is irreducible and of dimension 12 - 6 + 3 - 2 = 7.
--
-- II.4  Non-emptiness of the strata
-- It suffices to construct a member of N''_{1bar1bar2bar} since appropriate deformations will define elements of the other strata. We are free to choose values for s_0, s_1 != 0 and t_2 != 0, e.g., s_0 = s_1 = t_2 = 1 and, of course, t_0 = 1 and t_1 = -1.
special = map(QQ, A, {1,1,1,-1,1});
S' = QQ[gens S];
specialS = map(S', S, gens(S') | {1,1,1,-1,1});
-- We 'randomly' obtained the following octic form:
--J = specialS sub(idealFromKernel(N,f,params),S);
--fRand = randomElement J;
use S';
fRand = (81/8)*x^5*y^3-36*x^4*y^4-10*x^3*y^5+(59583/224)*x^2*y^6-(39045/112)*x*y^7+(26543/224)*y^8+(243/8)*x^5*y^2*z-(3327/280)*x^4*y^3*z-(93939/224)*x^3*y^4*z+(233721/280)*x^2*y^5*z-(779151/1120)*x*y^6*z+(29325/112)*y^7*z+(243/8)*x^5*y*z^2-(11517/280)*x^4*y^2*z^2+(77759/1120)*x^3*y^3*z^2-(102789/280)*x^2*y^4*z^2+(804807/1120)*x*y^5*z^2-(229681/560)*y^6*z^2+(81/8)*x^5*z^3-(53373/280)*x^4*y*z^3+(198099/224)*x^3*y^2*z^3-(13191/10)*x^2*y^3*z^3+(713367/1120)*x*y^4*z^3-(1737/80)*y^5*z^3-(35103/280)*x^4*z^4+(387041/1120)*x^3*y*z^4-(453051/1120)*x^2*y^2*z^4+(306627/1120)*x*y^3*z^4-(2863/32)*y^4*z^4-60*x^3*z^5+180*x^2*y*z^5-180*x*y^2*z^5+60*y^3*z^5;
-- First of all, it is singular only at P_0, P_1 and P_2:
expected = intersect apply(P,i->specialS prS i);
sing = ideal(jacobian matrix fRand)+fRand;
assert(radical sing == expected);
-- On the other hand, the singularities there are as desired:
assert isJ21(fRand, specialS prS T_0, x, y, z);
assert isJ21(fRand, specialS prS T_1, z, y, x);
-- Moving P_2 = (1;1;1) to (0;0;1):
fRand' = sub(fRand, {x=>x+z,y=>y+z});
assert isX10(fRand', y, x);
-- Therefore, fRand indeed defines a member of N''_{1bar1bar2bar}. This finishes the discussion about this configuration.
--
-- III  Both tangents pointing towards the quadruple-point
--    	(t_0 = t_1 = 1)
use A0;
A = A0/(t_0-1,t_1-1);
pr = map(A,A0);
S = A[gens S0];
prS = map(S,S0);
I = intersect(prS P33_0, prS P33_1, prS P_2^4);
(RS,inc,params,f) = universalFamily(8,I);
assert(#params == 13);
--
-- The degeneracy conditions:
M0 = directedDegConditions33Point(f,
    inc prS L_0, inc prS T_0, inc prS F_0, s_0);
M1 = directedDegConditions33Point(f,
    inc prS L_1, inc prS T_1, inc prS F_1, s_1);
M2 = tangencyCond(f,
    inc prS L_2,inc prS T_2,inc prS F_2,
    Multiplicity=>4, Degree=>2);
-- III.1  One degenerate singularity
assert(
    numgens target M0 == 2 and
    numgens target M1 == 2 and
    numgens target M2 == 2);
-- Investigating the loci where they are not surjective. By symmetry, it suffices to look at the matrices M0 (--> N'''_{11bar2}) and M2 (--> N'''_{112bar}).
--
-- III.1.1 N'''_{11bar2}
droppingRankCond = minors(2,mingens image M0);
assert(radical droppingRankCond == ideal(s_0));
-- The rank of M0 drops only if the second order vanishing condition on the [3;3]-point vanishes; but the distinguished tangent line also passes through a quadruple-point, which is impossible on a reduced octic. Therefore, N'''_{11bar2} is irreducible and of dimension 13 - 2 + 1 - 3 = 9.
--
-- III.1.2 N'''_{112bar}
droppingRankCond = minors(2,mingens image M2);
assert(radical droppingRankCond == ideal(t_2 * (t_2-1)));
-- The rank of M2 drops only if the special tangent of the quadruple-point points towards a [3;3]-point, which is impossible on a plane octic as long as it is reduced. Thus, also N'''_{112bar} is irreducible and 9-dimensional.
--
-- That both components are actually not empty will be part of III.4.
--
-- III.2  Two of the singularities degenerate
-- Again by symmetry, there are only two cases to consider.
M01 = M0 || M1;--> N'''_{1bar1bar2}
M02 = M0 || M2;--> N'''_{11bar2bar}
--
-- III.2.1 N'''_{1bar1bar2}
droppingRankCond = minors(4,mingens image M01);
assert(radical droppingRankCond == ideal(s_0 * s_1));
-- It is irrelevant that the rank drops, since it does so only if s_0 = 0 or s_1 = 0 and as explained in III.1.1, this requires the curve to be non-reduced. This implies that N'''_{1bar1bar2} is irreducible (if non-empty, which follows from III.4 below). Its dimension is 13 - 4 + 2 - 3 = 8.
--
-- III.2.2 N'''_{1bar1bar2}
droppingRankCond = minors(4,mingens image M02);
assert(radical droppingRankCond == ideal(s_0 * t_2 * (t_2-1)));
-- Comparing with III.1.1 and III.1.2, we see that here the locus where the rank drops is neglected and so this component (if not empty, cf. III.4 below) is irreducible and of dimension 8.
--
-- III.3  All three points degenerate
N = M0 || M1 || M2;
droppingRankCond = minors(6,mingens image N);
assert(radical droppingRankCond == ideal(s_0 * s_1 * t_2 * (t_2-1)));
-- Analogously as in III.2, we see that the locus where N is not of maximal rank is neglected in the stratum and so N'''_{1bar1bar2bar} is irreducible and of dimension 13 - 6 + 3 - 3 = 7; that it is in fact inhabited will be shown in the next section.
--
-- III.4  The strata are not empty
-- As before, it suffices to construct an element of the component N'''_{1bar1bar2bar}.
-- We may choose any parameters such that s_0,s_1,t_2,t_2-1 != 0. For example, s_0 = s_1 = 1, t_2 = -1; recall: t_0 = t_1 = 1.
special = map(QQ, A, {1,1,1,1,-1});
S' = QQ[gens S];
specialS = map(S', S, gens(S') | {1,1,1,1,-1});
-- We 'randomly' obtained the following octic form:
--J = specialS sub(idealFromKernel(N,f,params),S);
--fRand = randomElement J;
use S';
fRand = -(24/35)*x^5*y^3-9*x^4*y^4-(40/27)*x^3*y^5+(7/3)*x^2*y^6+(12296/315)*x*y^7-(5708/189)*y^8+(72/35)*x^5*y^2*z+(3/5)*x^4*y^3*z+(5/4)*x^3*y^4*z+(67181/420)*x^2*y^5*z-(141049/420)*x*y^6*z+(72227/420)*y^7*z-(72/35)*x^5*y*z^2+(375/7)*x^4*y^2*z^2-(17191/105)*x^3*y^3*z^2+(80/21)*x^2*y^4*z^2+(29299/105)*x*y^5*z^2-(17917/105)*y^6*z^2+(24/35)*x^5*z^3-(2553/35)*x^4*y*z^3+(177719/540)*x^3*y^2*z^3-(41603/84)*x^2*y^3*z^3+(73517/252)*x*y^4*z^3-(201521/3780)*y^5*z^3+(972/35)*x^4*z^4-(20831/126)*x^3*y*z^4+(13847/42)*x^2*y^2*z^4-(57643/210)*x*y^3*z^4+(51883/630)*y^4*z^4+(6/35)*x^3*z^5-(18/35)*x^2*y*z^5+(18/35)*x*y^2*z^5-(6/35)*y^3*z^5;
-- First of all, it is singular only at P_0, P_1 and P_2:
expected = intersect apply(P,i->specialS prS i);
sing = ideal(jacobian matrix fRand)+fRand;
assert(radical sing == expected);
-- On the other hand, the singularities there are as desired:
assert isJ21(fRand, specialS prS T_0, x, y, z);
assert isJ21(fRand, specialS prS T_1, z, y, x);
-- Moving P_2 = (1;1;1) to (0;0;1):
fRand' = sub(fRand, {x=>x+z,y=>y+z});
assert isX10(fRand', y, x);
-- Therefore, fRand indeed defines a member of N'''_{1bar1bar2bar}. This finishes the discussion about this last configuration.
--
-- EOF degenerate112.m2 ----------------------------------------------