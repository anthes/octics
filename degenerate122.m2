----------------------------------------------------------------------
-- M2-script studying plane octics with a [3;3]-point and two quadruple-points, at least one degenerate. That is, the following strata: N_{122bar}, N_{12bar^2}, N_{1bar22}, N_{1bar22bar}, N_{1bar2bar^2}.
--
-- Filename degenerate122.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
clearAll();
load "octicsFunctions.m2";
--
-- From 122.m2 we know that in any case, there will be disjoint components corresponding to whether one of the quadruple-point lies on the distinguished tangent line of the [3;3]-point. To limit the number of parameters, we go through this twice, once for each case.
--
-- The general set-up:
-- A0 = QQ[s,t_0,t_1,t_2] is the affine coordinate ring of rational affine 4-space AA^4,
-- S0 = A0[x,y,z] is the homogeneous coordinate ring of the trivial projective plane bundle over AA^4.
-- We consider the plane points P_0 = (0;0;1), P_1 = (1;0;0) and P_2 = (0;1;0) with distinguished tangent lines {y = t_0 x}, {z = t_1 y} and {x - t_2 z}, respectively.
--
-- In either case (quadruple-point on or off distinguished tangent), we can fix at least three points and two tangents. In each case, we put the [3;3]-point at P_2. Then t_2 != 0 gives to the component N' and t_2 = 0 gives rise to N''.
--
A0 = QQ[s,t_0,t_1,t_2];
S0 = A0[x,y,z];
L = {x,y,z};
T = {y-t_0*L_0,z-t_1*L_1,x-t_2*L_2};
F = {z,x,y};
P = apply(L,T,pair->trim ideal pair);
P33 = get33Ideal(L_2,T_2,F_2);
--
-- I  General distinguished tangent
-- First, we walk through this in general position, e.g., t_2 = 1, so that there is no quadruple-point on the distinguished tangent line of the [3;3]-point.
--
A = A0/(t_2-1);
pr = map(A,A0);
S = A[gens S0];
prS = map(S,S0);
--
I = intersect(prS P33, prS P_0^4, prS P_1^4);
(RS,inc,params,f) = universalFamily(8, I);
assert(#params == 13);
-- This is the universal family of octic forms where the associated curves have a [3;3]-point at P_2 with distinguished tangent line {x = z} and two quadruple-points off this line (at P_1 and P_2).
--
-- I.1  One degenerate singularity
-- We have to understand the kernels of the following matrices encoding the degeneracy conditions.
M0 = tangencyCond(f, inc prS L_0,inc prS T_0, inc prS F_0,
    Multiplicity=>4, Degree=>2);
M1 = tangencyCond(f, inc prS L_1,inc prS T_1, inc prS F_1,
    Multiplicity=>4, Degree=>2);
M2 = directedDegConditions33Point(f,
    inc prS L_2, inc prS T_2, inc prS F_2, s);
-- All three are of maximal rank 2 everywhere:
assert(numgens target M0 == 2 and isSurjective M0);
assert(numgens target M1 == 2 and isSurjective M1);
assert(numgens target M2 == 2 and isSurjective M2);
-- This implies that N'_{122bar} and N'_{1bar22} are irreducible of dimension 13 - 2 + 1 - 2 = 10. (Here, 13 - 2 is the rank of the kernel, +1 is for the parameter, either s or t_0, and 2 is the dimension of the stabiliser sub-group of GL(3,CC)). That it is indeed not empty will be shown in I.4 below.
--
-- I.2  Two degenerate singularities
-- Now we have to consider the common kernels of pairs of the above matrices:
M01 = M0 || M1;-- (both quadruple-points)
M02 = M0 || M2;-- (one quadruple-point and the [3;3]-point)
-- Again, they are of maximal rank 4 everywhere:
assert(isSurjective M01 and isSurjective M02);
-- Thus, N'_{12bar^2} and N'_{1bar22bar} are irreducible and of dimension 13 - 4 + 2 - 2 = 9. (See I.4 below for non-emptiness.)
--
-- I.3  All three degenerate (N'_{1bar2bar^2})
-- We consider the kernel of the common kernel of all three matrices:
N = M0 || M1 || M2;
assert(isSurjective N);
-- It has maximal rank 6 everywhere and so the stratum is irreducible of dimension 13 - 6 + 3 - 2 = 8. That it is indeed inhabited will be the subject of the next section.
--
-- I.4  The strata are non-empty
-- It remains to produce inhabitants. From our line of arguments, it follows that it is enough to construct an inhabitant in the case that all three points are degenerate and for fixed parameters, say s = 1, t_0 = -1 and t_1 = -1:
use A;
S' = QQ[gens S];
pr' = map(QQ, A, {1,-1,-1,1});
prS' = map(S', S, gens(S') | {1,-1,-1,1});
-- The following commented lines were used to obtain the hard-coded example fRand below:
--J = prS' sub(idealFromKernel(N,f,params),S);
--fRand = randomElement(J);
use S';
fRand = (16/45)*x^4*y^4-(21/4)*x^3*y^5+(2291/315)*x^4*y^3*z-(15/7)*x^3*y^4*z+(63/4)*x^2*y^5*z+(1304/63)*x^4*y^2*z^2-(2/15)*x^3*y^3*z^2-(5/3)*x^2*y^4*z^2-(63/4)*x*y^5*z^2+21*x^4*y*z^3-(1138/35)*x^3*y^2*z^3+(857/140)*x^2*y^3*z^3+(2627/315)*x*y^4*z^3+(21/4)*y^5*z^3+(758/105)*x^4*z^4+(2759/252)*x^3*y*z^4-(2927/630)*x^2*y^2*z^4-(2387/180)*x*y^3*z^4-(171/35)*y^4*z^4;
-- First of all, the singular locus is as small as possible:
expected = prS' prS intersect P;
sing = ideal jacobian matrix fRand;
assert(radical sing == expected);
-- Moreover, the singularities are degenerate, but in the mildest way they can, i.e., of type J_{2,1} or X_{10}, respectively.
assert(isJ21(fRand, prS' prS T_2, x, z, y));
assert(isX10(fRand, z, y));
assert(isX10(fRand, y, x));
-- Thus, the octic defined by fRand defines a member of the most degenerate stratum and an appropriate deformation defines a member of the other strata.
--
-- II  With special distinguished tangent
-- Setting t_2 = 0, the distinguished tangent direction at the [3;3]-point at P_2 points towards the quadruple-point at P_0.
use A0;
A = symbol A; S = symbol S; RS = symbol RS;
A = A0/(t_2);
pr = map(A,A0);
S = A[gens S0];
prS = map(S,S0);
--
I = intersect(prS P33, prS P_0^4, prS P_1^4);
(RS,inc,params,f) = universalFamily(8, I);
assert(#params == 14);
--
-- II.1  One degenerate singularity
-- If we let one quadruple-point degenerate, we have two cases to consider, namely, whether it is the one on the distinguished tangent or the other one. We denote by N''a_{122bar} be the component for the latter case (P_1 degenerate) and N''b_{122bar} for the former case (P_0 degenerate); analogously for N''_{1bar22bar} in II.2.
--
-- We have to understand the kernels of the following matrices encoding the degeneracy conditions.
M0 = tangencyCond(f, inc prS L_0,inc prS T_0, inc prS F_0,
    Multiplicity=>4, Degree=>2);
M1 = tangencyCond(f, inc prS L_1,inc prS T_1, inc prS F_1,
    Multiplicity=>4, Degree=>2);
M2 = directedDegConditions33Point(f,
    inc prS L_2, inc prS T_2, inc prS F_2, s);
-- Imposing degeneracy is independent for all parameters at the quadruple-points, but not at the [3;3]-point:
assert(numgens target M0 == 2 and isSurjective M0);
assert(numgens target M1 == 2 and isSurjective M1);
assert(numgens target M2 == 2 and not isSurjective M2);
-- This already shows that N''a_{122bar} and N''b_{122bar} are irreducible and of dimension 14 - 2 + 1 - 3 = 10. For N''_{1bar22}, we have to see what happens if the rank drops:
droppingRankCond = minors(2, mingens image M2);
assert(radical droppingRankCond == ideal s);
-- That is, the rank drops if and only if the second order direction of the degenerate [3;3]-point is trivial. In this case, the line has to be contained twice:
J = sub(idealFromKernel(sub(M2,{s=>0}),sub(f,{s=>0}),params),S);
assert((gens J)%(prS T_2)^2 == 0);
-- Thus, this case is neglected in the stratum anyways, It follows that the component N''_{1bar22} is irreducible and 10-dimensional as well.
--
-- That these three components are actually inhabited is content of Section II.4 below.
--
-- II.2  Two degenerate singularities
-- We have to investigate the kernels of the common kernels of pairs of the above matrices.
M01 = M0 || M1;--> N''_{12bar^2}
M02 = M0 || M2;--> N''b_{1bar22bar}
M12 = M1 || M2;--> N''a_{1bar22bar}
assert isSurjective M01;
-- Therefore, N''_{12bar^2} is irreducible and its dimension is 14 - 4 + 2 - 3 = 9. We will conclude the same for N''a_{1bar22bar} and N''b_{1bar22bar} once we have seen that their rank-dropping parameters are neglected anyways, for the same reason as in II.1:
droppingRankCond = minors(4,mingens image M02);
assert(radical droppingRankCond == ideal s);
droppingRankCond = minors(4,mingens image M12);
assert(radical droppingRankCond == ideal s);
--
-- That these three components are actually inhabited is content of Section II.4 below.
--
-- II.3  All three degenerate
-- We consider the kernel of the common kernel of all three matrices:
N = M0 || M1 || M2;
-- Of course, it is not surjective, but the locus where it is not is neglected for the same reason as in II.1:
droppingRankCond = minors(6,mingens image N);
assert(radical droppingRankCond == ideal s);
-- It follows that the component N''_{1bar2bar^2} is irreducible and of dimension 14 - 6 + 3 - 3 = 8.
--
-- II.4  The strata are non-empty
-- It remains to produce inhabitants. From our line of arguments, it follows that it is enough to construct an inhabitant in the case that all three points are degenerate and for fixed parameters, say s = 1, t_0 = -1 and t_1 = -1:
use A;
S' = QQ[gens S];
pr' = map(QQ, A, {1,-1,-1,0});
prS' = map(S', S, gens(S') | {1,-1,-1,0});
-- The following commented lines were used to obtain the hard-coded example fRand below:
--J = prS' sub(idealFromKernel(N,f,params),S);
--fRand = randomElement(J);
use S';
fRand = -(21/80)*x^4*y^4-(35/24)*x^3*y^5+(19/480)*x^4*y^3*z+(1/36)*x^3*y^4*z+(3/5)*x^4*y^2*z^2+(3/2)*x^3*y^3*z^2+(35/12)*x^2*y^4*z^2+(1/32)*x^4*y*z^3-(2/15)*x^3*y^2*z^3+(40/27)*x^2*y^3*z^3-(4/15)*x^4*z^4-(239/120)*x^3*y*z^4-(191/60)*x^2*y^2*z^4-(35/24)*x*y^3*z^4;
-- First of all, the singular locus is as small as possible:
expected = prS' prS intersect P;
sing = ideal jacobian matrix fRand;
assert(radical sing == expected);
-- Moreover, the singularities are degenerate, but in the mildest way they can, i.e., of type J_{2,1} or X_{10}, respectively.
assert(isJ21(fRand, prS' prS T_2, z, x, y));
assert(isX10(fRand, z, y));
assert(isX10(fRand, y, x));
-- Thus, the octic defined by fRand defines a member of the most degenerate stratum and an appropriate deformation defines a member of the other strata.
--
-- EOF degenerate122.m2 ----------------------------------------------