----------------------------------------------------------------------
-- M2-script to study octic curves in PP^2 with 3 quadruple-points, at least one of which is degenerate. Explicitly: N_{222bar}, N_{22bar^2} and N_{2bar^3}
--
-- Filename degenerate222.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
clearAll();
load "octicsFunctions.m2";
--
-- Notation:
-- A = QQ[t_0,t_1,t_2] is the coordinate ring of rational AA^3,
-- S = A[x,y,z] is the homogeneous coordinate ring of the trivial PP^2-family over AA^2. We will consider octics with certain singularities in the points P_i = V(L_i,T_i), i = 0,1,2, sometimes with a special or distinguished tangent line defined by T_i, where P_0 = (0;0;1), P_1 = (1;0;0), P_2 = (0;1;0) and T_0 = y - t_0 x, T_1 = z - t_1 y, T_2 = x - t_2 z.
--
-- It is enough to consider those tangents since up to automorphisms, the only configurations we miss are those where two degenerate quadruple-poins share a special tangent line, which is impossible on a reduced octic.
--
A = QQ[t_0,t_1,t_2];
S = A[x,y,z];
L = {x,y,z};
T = {y-t_0*L_0, z-t_1*L_1, x-t_2*L_2};
F = {z,x,y}; -- aux. localisation variables
P = apply(L,T,(f,g)->trim ideal(f,g));
P4 = apply(P,i->i^4);
Pd4 = apply(3,i->getDegenerateQuadrupleIdeal(L_i,T_i,F_i));
--
-- Preparation: The universal octic form with quadruple-points at P_0, P_1 and P_2:
(RS,inc,params,f) = universalFamily(8,intersect P4);
assert(#params == 15);
-- And the degeneracy conditions:
M = apply(3, i-> tangencyCond(f, inc L_i, inc T_i, inc F_i,
	Multiplicity=>4, Degree=>2));
assert all(M,m->numgens target m == 2);
--
-- I.1  Two ordinary and one degenerate quadruple-point (N_{222bar})
-- Imposing degeneracy at P_0 means taking the kernel of M_0.
assert isSurjective M_0;
-- Thus, the stratum N_{222bar} is irreducible and its dimension is computed as 15 - 2 + 1 - 3 = 11. (Here, -2 comes from the rank of M_0, +1 is since t_0 is a parameter and -3 is the dimension of the stabiliser sub-group of GL(3,CC) acting on the kernel.) It is easy to see that there actually exist such curves, but it also follows from I.4 below.
--
-- I.2  One ordinary and two degenerate quadruple-points (N_{22bar^2})
-- Imposing degeneracy at P_0 and P_1 means taking the kernel of
N = M_0 || M_1;
assert isSurjective N;
-- Therefore, N_{22bar^2} is irreducible as well and of dimension 15 - 4 + 2 - 3 = 10. (Non-empty by I.4 below.)
--
-- I.3  Three degenerate quadruple-points (N_{2bar^3})
N = M_0 || M_1 || M_2;
assert isSurjective N;
-- Therefore, N_{2bar^3} is irreducible as well and of dimension 15 - 6 + 3 - 3 = 9. (Inhabited by I.4 below.)
--
-- I.4  The strata are non-empty
-- From the line of arguments above it follows that appropriate deformations of a member of N_{2bar^3} will define members of the other two strata. We set t_0 = 1, t_1 = 2 and t_2 = 3; the choice is arbitrary. 
S' = QQ[gens S];
special = map(S',S,gens(S') | {1,2,3});
-- The next three (commented) lines were used to produce the 'random' element fRand hard-coded below.
--use S;
--J = sub(idealFromKernel(N,f,params),S);
--J' = special J;
--fRand = randomElement(J');
use S';
fRand = -(992/3)*x^4*y^4+(9364/15)*x^4*y^3*z-(10/9)*x^3*y^4*z-(4564/15)*x^4*y^2*z^2-(3/5)*x^3*y^3*z^2+(26800/3)*x^2*y^4*z^2+(7/5)*x^4*y*z^3+(3/5)*x^3*y^2*z^3+(2/15)*x^2*y^3*z^3-17858*x*y^4*z^3+18*x^4*z^4-(2692/45)*x^3*y*z^4+(2414/45)*x^2*y^2*z^4+(8/45)*x*y^3*z^4-12*y^4*z^4;
-- First of all, it is singular only at P_0, P_1 and P_2:
expected = intersect apply(P, i-> special i);
sing = ideal jacobian matrix fRand;
assert(radical sing == expected);
-- On the other hand, the quadruple-points at these points are degenerate, but as mild as possible, i.e., singularities of type X_{10}:
assert(isX10(fRand,x,y));
assert(isX10(fRand,z,y));
assert(isX10(fRand,z,x));
-- Thus, fRand defines an admissible member of N_{2bar^3}.
--
-- EOF degenerate222.m2 ----------------------------------------------