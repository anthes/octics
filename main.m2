----------------------------------------------------------------------
-- M2-script running multiple smaller files.
--
-- Filename: main.m2
--
-- Instructions: Load this file in Macaulay2 (tested in version 1.9.2)
-- via (input "main.m2";). If no errors occur, everything is fine.
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
-- In total, there are 47 strata, 37 of which parametrise reduced
-- curves. The study of the latter distributed over the following
-- files.
load "parameterFreeCases.m2";
load "upToTwoSingularities.m2";
load "threeNonDeg.m2";
load "1112.m2";
load "1111.m2";
load "degenerate222.m2";
load "degenerate122.m2";
load "degenerate112.m2";
load "degenerate111.m2";
--
-- The study of the non-reduced curves boils down to similar investi-
-- gations of sextics. This is what the next file does.
load "sextics.m2";
--
-- If the output reached this line, the code ran without failing
-- assertions.
--
-- EOF main.m2 -------------------------------------------------------