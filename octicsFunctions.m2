----------------------------------------------------------------------
-- M2-script supplying functions to investigate certain linear systems of curves in the plane.
--
-- Filename octicsFunctions.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
--  Ideals
--
-- Here are some basic ideals containing the constraints for affine plane singularities:
pointWithTangentAffine = (pt,l) -> (pt^2)+ideal(l);
getA1Affine = (pt) -> pt^2;
getA2Affine = (pt,t) -> pt^2+ideal(t);
getA3Affine = (pt,t) -> (pointWithTangentAffine(pt,t))^2;
getX9Affine = (pt) -> pt^4;
getX10Affine = (pt,t) -> pt^2*getA2Affine(pt,t);
getX11Affine = (pt,t) -> pt^2*getA3Affine(pt,t);
getJ10Affine = (pt,t) -> (pointWithTangentAffine(pt,t))^3;
--
-- Once we fix a pair of generators of the maximal ideal of a point, the number of equations can be reduced by the following explicit presentations, where {t = 0} is the distinguished tangent line.
pointWithTangentInAffineCoords = (l,t) -> ideal(t,l^2);
getA2InAffineCoords = (l,t) -> ideal(t^2,t*l^2,l^3);
getA3InAffineCoords = (l,t) -> ideal(t^2,t*l^2,l^4);
getTacNodeInAffineCoords = (l,t) -> getA3InAffineCoords(l,t);
getJ10InAffineCoords = (l,t) -> ideal(t^3,t^2*l^2,t*l^4,l^6);
get33InAffineCoords = (l,t) -> ideal(t^3,t^2*l^2,t*l^4,l^6);
getX9InAffineCoords = (x,y) -> (ideal(x,y))^2;
getX10InAffineCoords = (l,t)->getA2InAffineCoords(l,t)*(ideal(t,l))^2;
getX11InAffineCoords = (l,t)->getA3InAffineCoords(l,t)*(ideal(t,l))^2;
--
-- When we pass to the projective plane, we have to localise at a parameter (f, in the following) to use the above affine ideals:
getPointWithTangentIdeal = (l,t,f) -> (
-- We suppose that in the ring
    S := ring t;
    K := coefficientRing S;
-- l, t and f are forms generating the irrelevant ideal.
    assert((degree f)_0 == 1);
    assert(ideal(l,t,f) == ideal(basis(1,S)));
-- We localise away from f,
    Sloc := S[local u,Degrees=>{-1}]/(u*f-1);
    loc := map(Sloc,S);
-- resulting in affine coordinates t' = ut, l' = ul:
    A := K[local l',local t'];
    use Sloc;
    r := map(Sloc,A,{l*u,t*u});
    use A;
    localIdeal := pointWithTangentInAffineCoords(l',t');
    use S;
    return trim preimage(loc,r(localIdeal));
);
get33Ideal = (l,t,f) -> ( -- same as for getPointWithTangentIdeal
    S := ring t;
    assert((degree f)_0 == 1);
    assert(ideal(l,t,f) == ideal(basis(1,S)));
    Sloc := S[local u,Degrees=>{-1}]/(u*f-1);
    loc := map(Sloc,S);
    use Sloc;
    localIdeal := getJ10InAffineCoords(u*l,u*t);
    return trim preimage(loc,localIdeal);
);
getTacNode = (l,t,f) -> ( -- same as for getPointWithTangentIdeal
    S := ring t;
    assert((degree f)_0 == 1);
    assert(ideal(l,t,f) == ideal(basis(1,S)));
    Sloc := S[local u,Degrees=>{-1}]/(u*f-1);
    loc := map(Sloc,S);
    use Sloc;
    localIdeal := getA3InAffineCoords(u*l,u*t);
    return trim preimage(loc,localIdeal);
);
getDegenerateQuadrupleIdeal = (l,t,f) -> (
-- same as for getPointWithTangentIdeal
    S := ring t;
    assert((degree f)_0 == 1);
    assert(ideal(l,t,f) == ideal(basis(1,S)));
    Sloc := S[local u,Degrees=>{-1}]/(u*f-1);
    loc := map(Sloc,S);
    use Sloc;
    localIdeal := getX10InAffineCoords(u*l,u*t);
    return trim preimage(loc,localIdeal);
);
--
--  Functions handling families
--
-- The next function takes a list of elements of the same ring S and returns the free linear combination of the elements of that list. That is encoded in a quadruple consisting of a ring RS isomorphic to S[a_0,...,a_n] with the inclusion map S --> RS, the list of generators params = {a_0,...,a_n} and the element which is the linear combination of the list elements with coefficients a_i.
opts = {Variable => a};
familyFromList = opts >> o -> (l) -> (
    if (#l == 0) then return (ZZ,map(ZZ,ZZ),{},0);
    S := ring (l_0);
    A := coefficientRing S;
    RA := A[o.Variable_0..o.Variable_(#l-1)];
    params := gens RA;
    RS := RA[gens S];
    inc := map(RS,S);
    l' := apply(l,p->inc p);
    f := sum(apply(gens RA,l',(p,q)->p*q));
    return (RS,inc,params,f);
);
-- The above function will be used to generate the universal families of plane curves of a certain degree satisfying certain constraints which are encoded in terms of an ideal. Caution: it implicitly assumes that S is free over its coefficient ring!
opts = {Variable => a};
universalFamily = opts >> o -> (d,i) -> (
    S := ring i;
    A := coefficientRing S;
    mm := mingens trim ideal super basis(d,trim i);
    m := apply(rank source mm,j->mm_(0,j));
    if not isWeaklyHomogeneous(mm,d) then (
        mm = super basis(d,trim i);
    	m = apply(rank source mm,j->mm_(0,j));
    );
    assert(isWeaklyHomogeneous(mm,d));
    return familyFromList(m);
);
--
-- If K is a matrix with values in a free A-module A^n and if f is a free linear combination with n summands, then we can substitute the entries of the columns of K for the coefficients and get a ring element. The following function takes those elements and returns the ideal they generate.
idealFromImage = (K,f,params) -> (
    assert(#params == numgens target K);
    return ideal apply(numgens source K, j->
    	sub(f,apply(numgens target K,i->params_i=>K_(i,j))));
);
-- This is just the application of the former, choosing a generating set (matrix) for the kernel of M instead of M itself.
opts = {Mingens => false};
-- The boolean option Mingens forces the application of mingens instead of gens to get a generating set of the kernel.
idealFromKernel = opts >> o -> (M,f,params) -> (
    K := gens kernel M;
    if(o.Mingens) then K = mingens image K;
    return idealFromImage(K,f,params);
);
--
--  Classifying plane curve singularities:
--
-- Check if f has an ordinary n-fold point in the affine origin.
-- Caution: If the base ring is not a field, this function is insufficient to conclude non-degeneracy; if the ring is a domain, then it shows generic non-degeneracy (if true is returned).
isOrdinaryNFoldSingularityAffine = (n,f,u,v) -> (
-- We assume that f is a polynomial in a ring generated by u and v
    A := ring f;
    assert(set gens A === set {u,v});
-- First of all, we want f to be a proper n-fold point:
    if (f%(ideal(u,v))^n != 0) then return false;
    if (f%(ideal(u,v))^(n+1) == 0) then return false;
-- Finally, we check in both blow up charts
    sigma := map(A,A,{u=>u,v=>u*v});
    tau := map(A,A,{u=>u*v,v=>v});
-- that the strict transforms are reduced along the exceptional lines:
    tmp0 := sigma(f)//u^n;
    tmp0 = sub(tmp0,{u=>0});
    if discriminant(tmp0,v) == 0 then return false;
    tmp1 := tau(f)//v^n;
    tmp1 = sub(tmp1,{v=>0});
    if discriminant(tmp1,u) != 0 then return true;
    return false;
);
--
-- The following function returns the ideal containing the conditions that an affine n-fold singularity in the origin is degenerate.
degeneracyConditionsNFoldSingularity = (n,f,u,v) -> (
-- We assume that f is a polynomial in a ring generated by u and v
    A := ring f;
    assert(set gens A === set {u,v});
-- First of all, we want f to be a proper n-fold point:
    if (f%(ideal(u,v))^n != 0) then (error "1"; return false);
    if (f%(ideal(u,v))^(n+1) == 0) then (error "2"; return false);
-- Finally, we consider in both blow up charts
    sigma := map(A,A,{u=>u,v=>u*v});
    tau := map(A,A,{u=>u*v,v=>v});
-- the strict transforms and their discriminants along the exceptional line:
    tmp0 := sigma(f)//u^n;
    tmp0 = sub(tmp0,{u=>0});
    tmp0 = discriminant(tmp0,v);
    tmp1 := tau(f)//v^n;
    tmp1 = sub(tmp1,{v=>0});
    tmp1 = discriminant(tmp1,u);
    return sub(intersect(ideal(tmp0),ideal(tmp1)),coefficientRing A);
);
--
-- The next few functions are either applications of the above, or work analogously.
isOrdinaryQuadruplePointAffine = (f,u,v) ->
    isOrdinaryNFoldSingularityAffine(4,f,u,v);
isOrdinaryQuadruplePoint = (f,x,y,z) -> (
-- Preparing to use isOrdinaryQuadruplePointAffine
-- We assume that 
    S := ring f;-- is a polynomial ring with generators x,y,z
    assert(set gens S === set {x,y,z});
-- We ask whether f has an ordinary quadruple-point in the point x = y = 0:
    K := coefficientRing S;
    A := K[u,v];
    tmpMap := map(A,S,{x=>u,y=>v,z=>1}|apply(gens K,w->(w=>w)));
-- (A naive localisation map.)
    return isOrdinaryQuadruplePointAffine(tmpMap(f),u,v);
);
degeneracyConditionsQuadruplePoint = (f,x,y,z) -> (
-- Similar to isOrdinaryQuadruplePoint
    S := ring f;
    assert(set gens S === set {x,y,z});
    K := coefficientRing S;
    (KK,F) := flattenRing K;
    SS := K[x,y,z];
    ff := sub(f,SS);
    A := KK[u,v];
    tmpMap := map(A,SS,{u,v,1}); -- a naive localisation map
    use S;
    return preimage(F,
	degeneracyConditionsNFoldSingularity(4,tmpMap(ff),u,v));
);
degeneracyConditionsTriplePoint = (f,x,y,z) -> (
-- Similarly to isOrdinaryQuadruplePoint 
    S := ring f;
    assert(set gens S === set {x,y,z});
    K := coefficientRing S;
    (KK,F) := flattenRing K;
    SS := K[x,y,z];
    ff := sub(f,SS);
    A := KK[u,v];
    tmpMap := map(A,SS,{u,v,1}); -- a naive localisation map
    use S;
    return preimage(F,
	degeneracyConditionsNFoldSingularity(3,tmpMap(ff),u,v));
);
isOrdinaryTriplePointAffine = (f,u,v) ->
    isOrdinaryNFoldSingularityAffine(3,f,u,v);
isOrdinaryTriplePoint = (f,x,y,z) -> (
-- As isOrdinaryQuadruplePoint 
    S := ring f;
    assert(set gens S === set {x,y,z});
    K := coefficientRing S;
    A := K[u,v];
    tmpMap := map(A,S,{x=>u,y=>v,z=>1}); -- a naive localisation map
    return isOrdinaryTriplePointAffine(tmpMap(f),u,v);
);
isOrdinaryDoublePointAffine = (f,u,v) ->
    isOrdinaryNFoldSingularityAffine(2,f,u,v);
isOrdinaryDoublePoint = (f,x,y,z) -> (
-- As isOrdinaryQuadruplePoint
    S := ring f;
    assert(set gens S === set {x,y,z});
    K := coefficientRing S;
    A := K[u,v];
    tmpMap := map(A,S,{x=>u,y=>v,z=>1}|apply(gens K,w->(w=>w))); 
-- (A naive localisation map.)
    return isOrdinaryDoublePointAffine(tmpMap(f),u,v);
);
isOrdinary33Point = (f,t,x,y,z) -> (
-- Similar to isOrdinaryQuadruplePoint
    S := ring f;
    assert(not t%x%z == 0);
    f0 := sub(f,{z=>1});
    t0 := sub(t,{z=>1});
    sigma := map(S,S,{y=>x*y});
    f0 = sigma(f0)//x^2;
-- We keep one copy of the exceptional line and ask for a non-degenerate quadruple-point.
    t0 = sigma(t0)//x; -- => critical point = ideal(x,t0)
    tmp = inverse(map(S,S,{y=>t0}));
    f0 = tmp(f0);
    return isOrdinaryQuadruplePoint(f0,x,y,z);
);
degeneracyConditions33Point = (f,t,x,y,z) -> (
-- Similar to isOrdinary33Point
    S := ring f;
    assert(not t%x%z == 0);
    f0 := sub(f,{z=>1});
    t0 := sub(t,{z=>1});
    sigma := map(S,S,{y=>x*y});
    f0 = sigma(f0)//x^3;
    t0 = sigma(t0)//x; -- => critical point = ideal(x,t0)
    tmp = inverse(map(S,S,{y=>t0}));
    f0 = tmp(f0);
    return degeneracyConditionsTriplePoint(f0,x,y,z);
);
--
isD5Affine = (f) -> (
-- This works only if the strict transform splits over QQ.
-- We assume that f is a polynomial in a ring generated by two elements u and v:
    A := ring f;
    assert(#(gens A) == 2);
    (u,v) := toSequence gens A;
-- First of all, we want f to be a proper triple-point:
    if (f%(ideal(u,v))^3 != 0) then return false;
    if (f%(ideal(u,v))^4 == 0) then return false;
-- Next, we blow up.
    sigma := map(A,A,{u=>u,v=>u*v});
-- By construction, f' is divisible by u^3 (but not by u^4).
    strictTrans := (sigma f)//u^3;
-- What we have to show is that the strict transform meets {u = 0} in two different points, one tangentially and one transversely.
    exc := sub(strictTrans,{u=>0});
-- Reparametrise if the presentation is inappropriate.
    if degree(v,exc) != 3 then return isD5Affine(sub(f,{u=>u+v}));
    if discriminant(exc,v) != 0 then (
	return false;
    );-- else:
    factors := factor exc;
-- We have to assume that exc splits as h*g^2 with g,h of degree 1, possibly with a factor of degree 0
    degs := set select(apply(#factors,i->factors#i#1),i->(i!=0));
    if (degs === set {1, 2}) then (
-- Find f_2:
    	g := (select(factors,i->i#1 == 2))#0#0;
-- We want simple tangentiality to {u = 0} where g = 0:
    	if strictTrans%ideal(u,g^2) != 0 then (
	    return false;
	) else (
	    if strictTrans%ideal(u^2,g) != 0 then (
	    	return true;
	    ) else (
	        return false;
	    );
	);
    ) else (
		error("I can't decide, pardon me.");
		return false;
    );
    return false;
);
isJ21 = (f,t,x,y,z) -> (
-- We blow up once, localise and ask for a D_5-singularity.
    S := ring f;
    assert(set gens S === set {x,y,z});
    assert(not t%x%z == 0);
    f0 := sub(f,{z=>1});
    t0 := sub(t,{z=>1});
    sigma := map(S,S,{y=>x*y});
    f0 = sigma(f0)//x^3;
    t0 = sigma(t0)//x;
-- We move the critical point (x,t0) = 0 to (x,y) = 0:
    tmp = inverse(map(S,S,{y=>t0}));
    f0 = tmp(f0);
    A := (coefficientRing S)[local u,local v];
    loc := map(A,S,{x=>u,y=>v,z=>1});
    return isD5Affine(loc f0);
);
isX10Affine = (f) -> (
-- This works only if the quadratic part of the strict transform splits over QQ.
-- The procedure is similar to isJ21Affine.
    A := ring f;
    assert(#(gens A) == 2);
    (u,v) := toSequence gens A;
    if (f%(ideal(u,v))^4 != 0) then return false;
    if (f%(ideal(u,v))^5 == 0) then return false;
    sigma := map(A,A,{u=>u,v=>u*v});
    strictTrans := (sigma f)//u^4;
    exc := sub(strictTrans,{u=>0});
    if degree(v,exc) != 4 then return isX10Affine(sub(f,{u=>u+v}));
    if discriminant(exc,v) != 0 then (
	return false;
    );-- else:
    factors := factor exc;
    admissibleFactors := select(factors,i->(degree(v,i#0),i#1)==(1,2));
    if #admissibleFactors == 1 then (
	g := admissibleFactors#0#0;
	h := exc//(g^2);
-- f = (cst)*g^2*h where h has degree 2 in v.
    	if discriminant(h,v) == 0 then return false;-- else:
    	if strictTrans%ideal(u,g^2) != 0 then (
	    return false;
	) else (
	    if strictTrans%ideal(u^2,g) != 0 then (
	    	return true;
	    ) else (
	        return false;
	    );
	);
    ) else (
    	print("I can't decide, pardon me.");
	return false;
    );
);
isX10 = (f,x,y) -> (
-- Similar to isOrdinaryQuadruplePoint 
    S := ring f;
    z := sum((set gens S) - (set {x,y}));
    assert(set gens S === set {x,y,z});
    A := (coefficientRing S)[local u,local v];
    loc := map(A,S,{x=>u,y=>v,z=>1});
    return isX10Affine(loc f);
);
--
--  Procedures to derive certain degeneracy conditions:
--
opts = {Multiplicity=>1,Degree => 1};
tangencyCond = opts >> o -> (f,l,t,z) -> (
-- We assume that f is an element of a ring RS = A[params][l,t,z] where l, t and z are homogeneous of degree 1 (implicit assumption) and that f has multiplicity o.Multiplicity in (l,t) = 0.
    RS := ring f;
    params := gens coefficientRing RS;
    A := coefficientRing coefficientRing RS;
-- We localise away from z = 0 and identify the result with AA^2 such that (l,t) = 0 becomes the origin. By blowing up, we find the conditions that f is tangent to {t = 0} with multiplicity o.Degree.
    RSloc := RS[u,Degrees=>{ -1 }]/(u*z-1);
    loc := map(RSloc,RS);
    R := A[params][local t',local l'];
    r := map(RSloc,R,{u*(loc t),u*(loc l)});
    f' := (gens preimage(r,ideal loc f));
    assert(numgens source f' == 1 and numgens target f' == 1);
    f' = f'_(0,0);
    sigma := map(R,R,{t'*l',l'});
    strict := sigma(f')//(l'^(o.Multiplicity));
    exc := sub(strict,{l'=>0});
    toBeZero := exc%(t'^(o.Degree));
    toBeZeroCoeff := for t in terms toBeZero
    	list sub(leadCoefficient(t),RS);
-- The kernel of the following matrix parametrises the possible values for params so that if they get substituted into f, the resulting element satisfies the constraints discussed above.
    M := matrix for eq in toBeZeroCoeff list
               for g in params list sub(leadCoefficient(eq//g),A);
    return M;
);
--
opts = {Multiplicity => 1,Degree => 1};
secondOrderCond = opts >> o -> (f,l,t,z,s) -> (
-- Similar to tangencyCond, but blowing up once more.
    RS := ring f;
    params := gens coefficientRing RS;
    A := coefficientRing coefficientRing RS;
    RSloc := RS[u,Degrees=>{ -1 }]/(u*z-1);
    loc := map(RSloc,RS);
    R := A[params][local t',local l'];
    r := map(RSloc,R,{u*(loc t),u*(loc l)});
    f' := (gens preimage(r,ideal loc f));
    assert(numgens source f' == 1 and numgens target f' == 1);
    f' = f'_(0,0);
    sigma := map(R,R,{t'*l',l'});
    strict := sigma(f')//(l'^(o.Multiplicity));
    strict = sigma(strict)//(l'^(o.Multiplicity));
    exc := sub(strict,{l'=>0});
    toBeZero := exc%((t'-s)^(o.Degree));
    toBeZeroCoeff := for t in terms toBeZero
    	list sub(leadCoefficient(t),RS);
    M := matrix for eq in toBeZeroCoeff list
               for g in params list sub(leadCoefficient(eq//g),A);
    return M;
);
-- An immediate application: if f has a [3;3]-point in (l,t) = 0 with distinguished tangent line {t = 0}, then the following procedure returns the matrix encoding the conditions that the [3;3]-point is degenerate with specified second order information s.
directedDegConditions33Point = (f,l,t,z,s) -> 
    secondOrderCond(f,l,t,z,s,Multiplicity=>3,Degree=>2);
--
--  A few auxiliary functions:
--
-- The following should be used with care since it assumes the  coordinates of the ring are called x,y and z. 
idealFromCoords = (a,b,c) -> trim minors(2,matrix({{x,y,z},{a,b,c}}));
-- A function checking if a matrix is homogeneous of degree d in the first set of variables.
isWeaklyHomogeneous = (m,d) -> (
    e := listDeepSplice entries m;
    T := listDeepSplice(apply(e,i-> terms i));
    return all(T,t->(degree t)_0 == d);
);
-- A function flattening a list L.
listDeepSpliceAcc = (L,A) -> (
    if (not instance(L,List)) then return {L};
    if (#L == 0) then return A;
    return listDeepSpliceAcc(drop(L,1),A | listDeepSpliceAcc(L_0,{}));
);
listDeepSplice = (L) -> return listDeepSpliceAcc(L,{});
--
-- Some randomisation functions which are used to construct examples:
randomList = (n) -> apply(n,i->
    (2*random(0,1)-1)*(random(QQ^1,QQ^1))_(0,0));
randomVector = (n) -> diagonalMatrix(randomList(n))*random(QQ^n,QQ^1);
randomValue = (m) -> (m*randomVector(rank source m))_(0,0);
randomElement = (i) -> randomValue(matrix({i_*}));
randomLine = (x,y,z) -> (matrix({{x,y,z}})*randomVector(3))_(0,0);
--
-- EOF octicsFunctions.m2 --------------------------------------------