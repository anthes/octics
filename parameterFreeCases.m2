----------------------------------------------------------------------
-- M2-script studying (most of) the strata of the stratification which can be handled without parameters.
-- The strata are: N_2, N_{22}, N_{222}, N_{2^4}, N_1 and N_{11}. For later reference we also compute the dimensions of the components of the strata N_{12}, N_{122}, N_{112}, despite the fact that the proof that these components are actually distinct will be handled in other files. 
--
-- File name: ParameterFreeCases.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
clearAll();
load "octicsFunctions.m2";
--
-- I.  Ordinary quadruple points only
--
-- Notation:
-- S = QQ[x,y,z] is the homogeneous coordinate ring of the projective plane (over the rationals). We consider the following plane points: P_0 = (0;0;1), P_1 = (1;0;0), P_2 = (0;1;0), P_3 = (1;1;1) and P_4 = (1;1;0).
S = QQ[x,y,z];
P = { idealFromCoords(0,0,1), idealFromCoords(1,0,0),
	  idealFromCoords(0,1,0), idealFromCoords(1,1,1),
	  idealFromCoords(1,1,0) };
-- Ideals of quadruple-points:
P4 = apply(P,i->i^4);
--
-- In each case, it is easy to see that the strata are inhabited, so we omit the constructions od explicit constructions.
--
-- I.1  One quadruple-point (N_2) --- at P_0:
assert(35 == rank source super basis(8,P4_0));
-- Since we fixed a point, the stabiliser has dimension 6; thus, the stratum is of dimension 35 - 6 - 1 = 28.
--
-- I.2  Two quadruple-points (N_{22}) --- at P_0 and P_1:
assert(25 == rank source super basis(8,intersect(P4_{0,1})));
-- Since we fixed two points, the dimension of the stabiliser is 4 and the dimension of stratum is 25 - 4 - 1 = 20.
--
-- I.3  Three non-collinear quadruple-points (N_{222}) at P_0, P_1 and P_2:
assert(15 == rank source basis(8,intersect(P4_{0,1,2})));
-- The stabiliser of three non-collinear points is two-dimensional; hence, the dimension of the stratum is 15 - 2 - 1 = 12. 
--
-- I.4  Four quadruple-points (N_{2^4}) in general position, i.e., no three collinear --- at P_0,P_1,P_2 and P_3:
assert(5 == rank source super basis(8,intersect(P4_{0..3})));
-- There are only finitely many automorphisms fixing four points of which no three are collinear and so the stratum is of dimension 5 - 1 = 4.
--
-- Since on a reduced octic there are no three quadruple-points on a line, this covers all cases we have to consider.
--
-- II.   Non-degenerate [3;3]-Points only
--
-- Similar notation as before, but now we need to take of the distinguished tangent directions.
L = {y,z};
T = {x-y,y-z};
F = {z,x}; -- (aux. localisation variables)
Q = apply(2,i->trim ideal(L_i,T_i));
--
-- We will consider octics with [3;3]-points in Q_i = (L_i,T_i), i=0,1 with distinguished tangent line T_i, i=0,1 respectively. Explicitly, Q_i = P_i, i=0,1, with tangent lines defined by the linear forms T_0 = x - y and T_1 = y - z, respectively.
assert(Q == P_{0,1});
-- Ideals for [3;3]-points with prescribed tangent directions:
Q33 = apply(2,i->get33Ideal(L_i,T_i,F_i));
--
-- II.1  One [3;3]-point (N_{1}) --- at Q_0 = P_0
assert(33 == rank source super basis(8,Q33_0));
-- The stabiliser of a line and a point on the line is 5; therefore, the stratum has dimension 33 - 5 - 1 = 27.
--
-- II.2  Two [3;3]-points (N_{11}) in general position, i.e., none of the points lies on the other's distinguished tangent line:
assert(21 == rank source super basis(8,intersect(Q33)));
-- An automorphism preserving this configuration preserves exactly the line joining the Q_i and the two distinguished tangent lines; dually, this amounts to fixing three points. Thus, the stabiliser is of dimension 2; thus, as expected, the dimension of the stratum equals 21 - 2 - 1 = 18.
--
-- Since on a reduced octic the distinguished tangent of a [3;3]-point does not point towards another [3;3]-point, this is the only case we have to consider.
--
-- II.3  More [3;3]-points
--     	   ... need parameters!
-- The cases of three or four [3;3]-points will be considered in separate files. Concretely, 111.m2 deals with the case of three [3;3]-points and shows that there are two disjoint components, both of dimension 9. One corresponds to the case where the tangents are in general direction and the second is when they all lie on a conic. The file 1111.m2 helps proving that there is no plane octic with more than three [3;3]-points. The degenerate cases will be content of the files 1[bar]1[bar]1bar.m2 and upToTwoDegCases.m2.
--
-- III.   Mixed (non-degenerate) cases.
--
-- It will turn out that parameters are needed to understand the components of these strata, but we include a first approximation for sake of exposition.
--
-- Same notation as above. Note that (only) P_0, P_3 and P_4 are on the line T_0 and on the line T_1 there lie P_1 and P_3 (only):
assert all({0,3,4}, i->(T_0%P_i) == 0); -- => P_0,P_3,P_4 on T_0
assert all({1,3}, i->(T_1%P_i) == 0); -- => P_1,P_2 on T_1
assert all({1,2}, i->(T_0%P_i) != 0); -- => P_1,P_2 not on T_0
assert all({0,2,4}, i->(T_1%P_i) != 0); -- => P_0,P_2,P_4 not on T_1
--
-- III.1  Two mixed singularities.
--
-- III.1.1 One quadruple- and one [3;3]-point (N_{12})
--
-- In 12.m2, we will see that there are two disjoint components which we denote by N'_{12} and N''_{12}.
--
-- In general position (N'_{12}), i.e., the distinguished tangent of the [3;3]-point (at Q_1) misses the quadruple point (at P_0):
m0 = super basis(8,intersect(P4_0,Q33_1));
assert(23 == rank source m0);
-- Preserving a line, a point on, and a point off the line, we get a stabiliser sub-group of dimension 3; hence, the dimension of (this component of) the stratum is computed as 23 - 3 - 1 = 19.
--
-- In special position (N''_{12}), that is, the distinguished tangent [3;3]-point (at Q_1) points towards the quadruple-point (at P_3):
m1 = super basis(8,intersect(P4_3,Q33_1));
assert(24 == rank source m1);
-- The dimension of this extra component is 24 - 4 - 1 = 19 (as well) since preserving a line through a pair of points is equivalent to fixing the two points, resulting in a stabilising projective subgroup of dimension 4.
--
-- III.2  Three mixed singularities.
--
-- III.2.1 Two quadruple- and one [3;3]-point (N_{122})
--
-- In 122.m2 we will see that there are two disjoint components which we denote by N'_{122} and N''_{122}. They arise as follows:
--
-- In general position (N'_{122}), i.e., the distinguished tangent of the [3;3]-point points towards neither of the quadruple-points:
assert(13 == rank source super basis(8,intersect(Q33_0,P4_1,P4_2)));
-- The configuration under consideration is fixed by precisely those automorphisms preserving the three concurrent lines meeting in Q_0 and the line joining P_1 and P_2; thus, the stabiliser sub-group is of dimension 1 and so (this component of) the stratum is 11-dimensional.
--
-- In special position (N''_{122}), that is, the quadruple-point at P_3 lies on the distinguished tangent line of the [3;3]-point, but the other quadruple-point (at P_1) does not:
assert(14 == rank source super basis(8,intersect(Q33_0,P4_1,P4_3)));
-- Again, we can already compute the dimension of this extra component as 14 - 2 - 1 = 11 since the automorphisms fixing the configuration are those fixing the three points, which has two-dimensional stabiliser sub-group (in PGL(3,CC)).
--
-- Since there are no two quadruple-points and a [3;3]-point of an octic curve on one line, there are no more cases to consider.
--
-- III.2.2 Two [3;3]- and one quadruple-point (N_{112})
--
-- As we will see in 112.m2, this stratum has three pair-wise disjoint components N'_{112}, N''{112} and N'''_{112} arising as follows.
--
-- N'_{112}: In general position, i.e., the distinguished tangents of the [3;3]-points (Q_0, Q_1) both miss the quadruple-point (at P_2):
assert(11 == rank source super basis(8,intersect(Q33_0,Q33_1,P4_2)));
-- Again, we compute the dimension of (this component of) the stratum: The stabiliser of this configuration is finite since besides the three points fixed already, we automatically fix a fourth (in general position) as the intersection point of the two distinguished tangent lines. Thus, the dimension is 11 - 1 = 10.
--
-- N''_{112}: The quadruple-point (P_4) lies on the distinguished tangent line of only one of the [3;3]-points (at Q_0):
assert(12 == rank source super basis(8,intersect(Q33_0,Q33_1,P4_4)));
--
-- N'''_{112}: The quadruple-point (at P_3) lies on the distinguished tangent lines of both [3;3]-points (at Q_0, Q_1):
assert(13 == rank source super basis(8,intersect(Q33_0,Q33_1,P4_3)));
--
-- Both extra components have dimension 10 as well: The dimension of the stabiliser of the configuration is 1 or 2, resp., since the distinguished tangent line(s) at Q_0 (and Q_1) are given by the line joining P_4 and Q_0 (and Q_1, respectively). Therefore we get 12 - 1 - 1 = 13 - 2 - 1 = 10 as the dimension.
--
-- Since it cannot happen that two [3;3]- and one quadruple-point are collinear, there is nothing more to check.
--
-- III.3  Four mixed singularities.
--     	   ... need parameters!
--
-- The only possible case (one quadruple- and three [3;3]-points) will be considered in the separate file 1112.m2. We will see that the stratum has two disjoint components of dimension one.
--
-- IV.   Degenerate singularities
--
-- The degenerate cases (with or without) parameters will be handled in other files.
--
-- EOF parameterFreeCases.m2 -----------------------------------------