----------------------------------------------------------------------
-- M2-script to study sextic curves in PP^2 with quadruple- and [3;3]-points
--
-- Filename sextics.m2
----------------------------------------------------------------------
--
clearAll();
load "octicsFunctions.m2";
--
S = QQ[s][x,y,z];
--
L = {x,x};
T = {y,z};
F = {z,y}; -- (aux. localisation variables)
P = apply(2,i->trim ideal(L_i,T_i));
P33 = apply(2,i->get33Ideal(L_i,T_i,F_i));
--
-- I  Quadruple-points
--
-- I.1  One ordinary quadruple-point
assert(numgens source super basis(6,P_0^4) == 18);
-- The stabiliser group of a point on the plane is of dimension 6. Therefore, the dimension of the space of sextics with a  quadruple-point is 18 - 6 - 1 = 11. Clearly, there exist admissible sextics with an ordinary quadruple-point, e.g., a union of four concurrent, and two general lines.
--
-- I.2  One degenerate quadruple-point
I = getDegenerateQuadrupleIdeal(L_0,T_0,F_0);
assert(numgens source super basis(6,I) == 16);
-- Since the group of automorphisms preserving the point and a tangent direction is of dimension 5, we conclude that the dimension of the space of sextics with a degenerate quadruple-point is 16 - 5 - 1 = 10.
-- Admissible sextics with a degenerate quadruple-point are easily constructed; e.g., the union of a cuspidal cubic, two general lines  through the cusp, and a general line missing the cusp.
--
-- Since on a sextic there are no two quadruple-points, this is all we have to consider here.
--
-- II  [3;3]-points
--
-- II.1  One non-degenerate [3;3]-point
assert(numgens source super basis(6,P33_0) == 16);
-- Since the group of automorphisms preserving the point and a tangent direction is of dimension 5, we conclude that the dimension of the space of sextics with an ordinary [3;3]-point is 16 - 5 - 1 = 10. It is easy to construct an admissible sextic with an ordinary [3;3]-point; e.g., the union of three general conics with a common point and common tangent in this point.
--
-- II.2  One degenerate [3;3]-point
(RS,inc,params,f) = universalFamily(6,P33_0);
assert(#params == 16);
M = directedDegConditions33Point(f,inc L_0, inc T_0, inc F_0, s);
assert(isSurjective M and numgens target M == 2);
-- Thus, the space of sextics with a degenerate [3;3]-point in P_0 has dimension 14 + 2 - 1 = 15 and so the dimension of the moduli space of sextics with a degenerate [3;3]-point is irreducible and of dimension 15 - 6 = 9.
-- Constructing examples:
-- For every possible second order direction, there exists a (possibly reducible) quadric with an according A5-singularity; take the union of such quartics with a general conic passing through the point in prescribed tangent direction.
-- We have to produce two examples; one, where the second order information is trivial along the tangent, and one where it is not. For the former, take a smooth cubic and the tangent line of a flex point, as well as a general conic through the same point, being tangent there as well. For the general case, we can take the union of three conics with the same tangent in a single point, but where (at least) two of them also agree up to second order.
--
-- II.3  Two non-degenerate [3;3]-points
-- This case can be dealt with abstractly: We know that the two [3;3]-points have to be in general position so that there is  (exactly) a pencil of conics through the configuration and every sextic is a union of three pair-wise distinct such conics. Therefore, space of such sextics is 3-dimensional, but the group of stabilising automorphisms has dimension 2, leaving one dimension for the space of such sextics. Let us quickly confirm this:
assert(numgens source super basis(6,intersect(P33)) == 4);
-- Hence, the dimension is 4 - 2 - 1 = 1.
--
-- There are no more cases we have to consider.
--
-- EOF sextics.m2 ----------------------------------------------------