----------------------------------------------------------------------
-- M2-script to study the strata N_{122}, N_{112} and N_{111}
--
-- Filename threeNonDeg.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
-- We will deal with the three strata in three different sections, each with its own notation.
-- 
-- I  One [3;3]- and two quadruple-points (N_{122})
--  
clearAll();
load "octicsFunctions.m2";
--
-- Notation:
-- A = QQ[t] is the coordinate ring of the rational line AA^1.
-- S = A[x,y,z] is the homogeneous coordinate ring of the trivial PP^2-family over AA^1.
-- We will consider octics with quadruple-points in P_0 = (0;0;1) and P_1 = (1;0;0) and a [3;3]-point in P_2 = (0,1,0) with varying distinguished tangent line defined by T_2 = x-tz.
--
-- We know that there will be one extra modulus of octics when t = 0. We aim to prove that this is the only instance where something unexpected is happening and that the two resulting families have no reduced octic in common. More precisely, the intersection corresponds to the octics containing the line defined by x twice.
--
-- Each component will be of dimension 11; this was computed in Sec. III.2.1 of parameterFreeCases.m2.
--
A = QQ[t];
S = A[x,y,z];
L = {x,y,z};
T = {y,z,x-t*z};
F = { , ,y}; -- aux. localisation variables
-- The ideals of the reduced points:
P = apply(3,i->trim ideal(L_i,T_i));
-- The ideals with the constraints for the desired singularities:
P4 = apply(2,i->P_i^4);
P33 = get33Ideal(L_2,T_2,F_2);
-- The relevant ideals and the universal family of octics with a [3;3]-point in P_2 with distinguished tangent line T_2:
Iconstant = intersect(P4);
Ivariable = P33;
(RS,inc,params,f) = universalFamily(8,Ivariable);
assert(#params == 33);
use RS;
-- Reduction of f modulo the remaining conditions:
toBeZero = f%(inc Iconstant);
toBeZeroCoeff = for Term in terms toBeZero list leadCoefficient(Term);
M = matrix for eq in toBeZeroCoeff
		    list for g in params list leadCoefficient(eq//g);
-- The kernel of M is the space of coefficients for which f is as desired for general t; its dimension is 13 = 33 - 20, as expected:
assert(numgens target M == 20);
droppingRankCond = minors(20,mingens image M);
assert(radical droppingRankCond == ideal(t));
-- Thus, the rank only drops for t = 0; indeed:
assert(numgens kernel sub(M,{t=>0}) == 14);
--
-- It remains to study the intersection to see that we have two disjoint components. The argument is analogous so 111.m2, e.g.
Q = S/(t);
pr = map(Q,S);
use S;
J = ideal super basis(8,intersect(P4_0,P4_1,P33));
use Q;
J' = pr J;
Jspecial = ideal super basis(8,intersect(pr(P4_0),pr(P4_1),pr(P33)));
-- Sanity check:
assert isSubset(J',Jspecial);
-- However:
assert((gens J')%(pr T_2)^2 == 0);-- but
assert((gens Jspecial)%(pr T_2)^2 != 0);
-- This means that the degenerations of curves in N'_{122} towards N''_{122} contain the distinguished tangent line twice. Therefore, the components are disjoint, as claimed.
--
-- That both components are inhabited can be seen by means of basic geometric constructions. For example, four conics through P_0 and P_1, of which three also pass through P_2, all three with the same tangent direction at P_2, for the general component, and for the special component, take a nodal cubic with the node in P_0, let P_2 be a general point of the cubic, T_2 its tangent line, and P_1 the other intersection point of the line with the cubic; then consider the union of the nodal cubic, the tangent line, the line joining P_0 and P_1, and a general cubic through the configuration.
--
-- II  Two [3;3]-points and one quadruple-point
clearAll();
load "octicsFunctions.m2";
--
-- From the calculations in parameterFreeCases.m2 III.2.2, we know that there are possibly three components, depending on how many of the two distinguished tangent lines pass through the quadruple-point. This script shows that the three components N'_{112} (where both lines miss the quadruple-point), N''_{112} (where exactly one of the lines passes through the quadruple-point) and N'''_{112} (where the quadruple-point sits at the intersection of both lines) are irreducible and pair-wise disjoint. They are of dimension 10 by parameterFreeCases.m2 III.2.2. 
--
-- Notation:
-- A = QQ[t_0,t_1] is the coordinate ring of the rational plane AA^2.
-- S = A[x,y,z] is the homogeneous coordinate ring of the trivial PP^2-family over AA^2.
-- We will consider octics with two [3;3]-points in P_0 = (0;0;1) and P_1 = (1;0;0), with varying distinguished tangent lines defined by the linear forms T_0 = x - t_0 y and T_1 = y - t_1 z, and a quadruple-point in P_2 = (1;1;1).
--
A = QQ[t_0,t_1];
S = A[x,y,z];
L = {y,z,x-y};
T = {x-y*t_0,y-z*t_1,y-z};
F = {z,x,}; -- aux. localisation variables
-- The ideals of the reduced points:
P = apply(3,i->trim ideal(L_i,T_i));
-- The ideals with the constraints for the desired singularities:
P33 = apply(2,i->get33Ideal(L_i,T_i,F_i));
P4 = P_2^4;
Iconstant = P4;
Ivariable = intersect P33;
-- Building the universal family of octics with two [3;3]-points with distinguished tangents given by the linear forms T_0, T_1.
(RS,inc,params,f) = universalFamily(8, Ivariable);
assert(#params == 21);
-- Reduction of f modulo the quadruple-point conditions:
toBeZero = f%inc(Iconstant);
toBeZeroCoeff = for Term in terms toBeZero list leadCoefficient(Term);
M = matrix for eq in toBeZeroCoeff
		    list for g in params list leadCoefficient(eq//g);
-- Generically, the rank of M is 10, but it drops if t_i = 1:
assert(numgens target M == 10);
droppingRankCond = minors(10,mingens image M);
assert(radical droppingRankCond == ideal((t_0-1)*(t_1-1)));
M1 = sub(M,{t_1=>1});
M2 = sub(M,{t_0=>1,t_1=>1});
assert(numgens kernel M1 == 12);
assert(numgens kernel M2 == 13);
--
-- It remains to compare the different components. For this, we need parametrisations of the different families. The kernels of the matrices above define different instances of f, and those give rise to the desired parametrisations:
use A;
A' = A/(t_1-1);
pr' = map(A',A);
S' = A'[gens S];
prS' = map(S',S);
RS' = S'[params];
prRS' = map(RS',RS);
A'' = A'/(t_0-1);
pr'' = map(A'',A');
S'' = A''[gens S'];
prS'' = map(S'',S');
RS'' = S''[params];
prRS'' = map(RS'',RS');
-- The general component and its degenerations as t_1 -> 1:
J = sub(idealFromKernel(M,f,params),S);
J' = prS' J;
-- The first special component and its degenerations as t_0 -> 1:
Jsp1 = sub(idealFromKernel(pr' M, prRS' f, gens RS'),S');
Jsp1' = prS'' Jsp1;
-- The last special component:
Jsp2 =sub(idealFromKernel(pr'' pr' M, prRS'' prRS' f, gens RS''),S'');
-- Sanity check: 
assert isSubset(J',Jsp1);
assert isSubset(Jsp1',Jsp2);
-- Every limit of the general component along the first special component contains the line T_1 twice, but the elements of the first special component do not:
assert((gens J')%(prS' T_1)^2 == 0);
assert((gens Jsp1)%(prS' T_1)^2 != 0);
--
-- Similarly, the limits of the first special component along the second special component contain the line T_0 twice, but the actual elements of the second special component do not:
assert((gens Jsp1')%(prS'' prS' T_0)^2 == 0);
assert((gens Jsp2)%(prS'' prS' T_0)^2 != 0);
--
-- As claimed. Since when passing to N_{112} we neglect non-reduced curves, the pair-wise intersections are not contained and so the three components of the stratum are pair-wise disjoint.
--
-- It is easy to come up with geometric constructions of inhabitants for the general component (t_0 != 1 != t_1) or the second special component (t_0 = t_1 = 1). Since this is a bit harder for the first special component, (t_1 != 1 and t_0 general), we show that the following form defines an inhabitant in that case.
use A';
R = A'/(t_0-1/2);-- isomorphic to QQ
q = map(R,A);
Q = R[gens S];
qQ = map(Q,S);
qQ' = map(Q,S');
expected = qQ intersect(P);
f = 2*x^5*y^3-(24/5)*x^4*y^4-(20/27)*x^3*y^5+(4/9)*x^2*y^6-(87497/840)*x*y^7+(1179841/7560)*y^8-6*x^5*y^2*z+(159/35)*x^4*y^3*z+(28/135)*x^3*y^4*z+(12272/45)*x^2*y^5*z-(987109/2520)*x*y^6*z-(1150217/15120)*y^7*z+6*x^5*y*z^2-(52/5)*x^4*y^2*z^2+(21667/630)*x^3*y^3*z^2-(105043/315)*x^2*y^4*z^2+(530869/630)*x*y^5*z^2-(304831/1260)*y^6*z^2-2*x^5*z^3+(923/35)*x^4*y*z^3-(201223/1890)*x^3*y^2*z^3-(923/20)*x^2*y^3*z^3-(122039/630)*x*y^4*z^3+(908081/7560)*y^5*z^3-(110/7)*x^4*z^4+(30785/378)*x^3*y*z^4+(1305/14)*x^2*y^2*z^4-(73805/504)*x*y^3*z^4+(8795/216)*y^4*z^4-(53/6)*x^3*z^5+(53/4)*x^2*y*z^5-(53/8)*x*y^2*z^5+(53/48)*y^3*z^5;
--
-- Claim: The octic curve defined by f is a member of the component indicated above.
-- Since
assert(f%qQ'(Jsp1) == 0);--, the curve has at least the desired singularities; in fact, it has no more than the three singularities that we imposed:
sing = ideal jacobian matrix f;
assert(radical sing == expected);
-- and the three are ordinary, as claimed:
assert(isOrdinaryQuadruplePoint(sub(f,{x=>x+z,y=>y+z}),x,y,z));
assert(isOrdinary33Point(f,qQ T_0,y,x,z));
assert(isOrdinary33Point(f,qQ T_1,z,y,x));
-- This proves the claim and concludes this section.
--
-- III  Three [3;3]-points
--
clearAll();
load "octicsFunctions.m2";
--
-- Notation:
-- A = QQ[t] is the coordinate ring of the rational line AA^1.
-- S = A[x,y,z] is the homogeneous coordinate ring of the trivial PP^2-family over AA^1.
-- We will consider octics with [3;3]-points in P_i = V(L_i,T_i), i = 0,1,2, with distinguished tangent line V(T_i), respectively.
-- Concretely, P_0 = (0;0;1), P_1 = (1;0;0), P_2 = (0;1;0) and
--     	       T_0 = x - ty,  T_1 = y - z,   T_2 = x - z
--
-- The line we miss (T_0 = y) is irrelevant since it passes through P_1 and on a reduced octic, the distinguished tangent line of a [3;3]-point does not pass through another [3;3]-point.
--
A = QQ[t];
S = A[x,y,z];
L = {y,z,x};
T = {x-t*y,y-z,x-z};
F = {z,x,y}; -- aux. localisation variables
P = apply(3,i->trim ideal(L_i,T_i));
-- The ideals containing the [3;3]-point-constraints:
P33 = apply(3,i->get33Ideal(L_i,T_i,F_i));
-- The following ideal gives the constraints for [3;3]-points in P_1 and P_2 with distinguished tangents as above.
Iconstant = intersect(P33_1,P33_2);
-- The following is the ideal for a [3;3]-point in P_0 whose distinguished tangent along the line {x = ty}, thus depending on t.
Ivariable = P33_0;
-- The family of octics fulfilling the variable constraints:
(RS,inc,params,f) = universalFamily(8,Ivariable);
assert(#params == 33);
-- Reduction of f modulo the remaining conditions:
toBeZero = f%(inc Iconstant);
toBeZeroCoeff = for Term in terms toBeZero list leadCoefficient(Term);
M = matrix for eq in toBeZeroCoeff
		    list for g in params list leadCoefficient(eq//g);
-- M is a Matrix A^24 <-- A^33, generically of maximal rank, leaving 33 - 24 = 9 generic dimensions:
assert(numgens target M == 24);
droppingRankCond = minors(24, mingens image M);
assert(radical droppingRankCond == ideal(t+1));
-- Thus, the only instance where the rank drops is where t = -1, in which case the three tangents are on the conic V(xy-xz-yz).
--
-- If t != -1, we get an irreducible component N'_{111} (once we have shown that there indeed exist admissible octics so that it is not empty) and it is of dimension 9 + 1 - 1 = 9.
--
-- In this case, the kernel has dimension 10:
prA = map(QQ,A,{t=>-1});
Q = QQ[gens S];
pr = map(Q,S, (gens Q) | {-1});
assert(numgens kernel prA M == 10);
--
-- We now show that the intersection of the two components parametrises the octics containing the conic V(xy-xz-yz) twice. The notation is the same as before, but we specialise to t = -1.
use S;
-- The ideal of octics for t != -1:
J = ideal super basis(8,intersect P33);
-- The ideal of their limits as t -> -1:
J' = pr J;
use Q;
-- The ideal of octics for t = -1:
Jspecial = ideal super basis(8,intersect apply(P33,i->pr i));
-- Sanity check:
assert isSubset(J',Jspecial);
-- However:
conic = x*y - x*z - y*z;
assert((gens J')%conic^2 == 0);-- but
assert((gens Jspecial)%conic^2 != 0);
-- Hence, the intersection of the closures of the strata consist of non-reduced curves only. In particular, if inhabited, we get an extra component N''_{11} which is disjoint from the other component but of the same dimension 10 + 0 - 1 = 9.
--
-- Finally, we have to show that both components contain admissible curves with three non-degenerate [3;3]-points.
-- We begin with the component of general distinguished tangents. The notation is the same as before, but with a fixed value for t.
use S;
rand = 7/10;
QRand = QQ[gens S];
prRand = map(QRand,S,append(gens QRand,rand));
sing = intersect(apply(P,i->prRand(i)));
f = -15*x^5*y^3 + 3*x^4*y^4 - (119/50)*x^3*y^5 + 45*x^5*y^2*z - (46/35)*x^4*y^3*z - (223/1000)*x^3*y^4*z + (357/50)*x^2*y^5*z - 45*x^5*y*z^2 - (162/35)*x^4*y^2*z^2 + (137353/21000)*x^3*y^3*z^2 - (18613/1500)*x^2*y^4*z^2 - (357/50)*x*y^5*z^2 + 15*x^5*z^3 + (6/5)*x^4*y*z^3 - (3803/140)*x^3*y^2*z^3 - (8131/750)*x^2*y^3*z^3 + (40459/3000)*x*y^4*z^3 + (119/50)*y^5*z^3 + (61/35)*x^4*z^4 + (10319/525)*x^3*y*z^4 - (18973/500)*x^2*y^2*z^4 + (546/25)*x*y^3*z^4 - (2891/750)*y^4*z^4 + (25/7)*x^3*z^5 - (15/2)*x^2*y*z^5 + (21/4)*x*y^2*z^5 - (49/40)*y^3*z^5;
--
-- Claim: The octic defined by f has only non-degenerate [3;3]-points, located at P_0, P_1 and P_2.
--
assert(f%prRand J == 0);
assert(radical ideal singularLocus ideal f == sing);
-- Therefore, the singular locus is supported exactly at the [3;3]-points we imposed.
--
-- We have to check that they are all non-degenerate [3;3]-points.
assert(isOrdinary33Point(f,prRand(T_0),x,y,z));
assert(isOrdinary33Point(f,prRand(T_1),z,y,x));
assert(isOrdinary33Point(f,prRand(T_2),x,z,y));
--
-- This proves the claim. In particular, N'_{111} is inhabited.
--
-- We now complete the discussion of three [3;3]-points by considering the extra component corresponding to octics where the distinguished tangents of the [3;3]-points are on a conic. We have to show that there exists such an octic with non-degenerate [3;3]-points.
--
-- Instead of the following 'random' element, we could also take the union of the conic and two general cubics tangent to the conic in P_0, P_1 and P_2.
--
-- The notation is the same as earlier, specialised to t = -1.
use Q;
sing = intersect(apply(P,i->pr(i)));
f = -(9/20)*x^5*y^3 - (8/3)*x^4*y^4 - (4/3)*x^3*y^5 + (27/20)*x^5*y^2*z + (149/15)*x^4*y^3*z - 8*x^3*y^4*z + 4*x^2*y^5*z - (27/20)*x^5*y*z^2 - (333/20)*x^4*y^2*z^2 + (18023/540)*x^3*y^3*z^2 + (1186/27)*x^2*y^4*z^2 - 4*x*y^5*z^2 + (9/20)*x^5*z^3 + (85/6)*x^4*y*z^3 - (9077/270)*x^3*y^2*z^3 - (55001/540)*x^2*y^3*z^3 - (1436/27)*x*y^4*z^3 + (4/3)*y^5*z^3 - (287/60)*x^4*z^4 - (3017/270)*x^3*y*z^4 + (2147/180)*x^2*y^2*z^4 + (1721/45)*x*y^3*z^4 + (538/27)*y^4*z^4 + (83/4)*x^3*z^5 + (249/4)*x^2*y*z^5 + (249/4)*x*y^2*z^5 + (83/4)*y^3*z^5;
--
-- Claim: The octic V(f) has only non-degenerate [3;3]-singularities, located in the points P_0, P_1 and P_2.
--
assert(f%Jspecial == 0);
assert(radical ideal singularLocus ideal f == sing)
-- Thus, the singular locus consists precisely of the [3;3]-points we
-- imposed. By the following three lines, they are non-degenerate:
assert(isOrdinary33Point(f,pr(T_0),x,y,z));
assert(isOrdinary33Point(f,pr(T_1),z,y,x));
assert(isOrdinary33Point(f,pr(T_2),x,z,y));
--
-- This proves the claim. In particular, N''_{111} is inhabited, too.
--
-- EOF threeNonDeg.m2 ------------------------------------------------