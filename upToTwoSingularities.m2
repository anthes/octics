----------------------------------------------------------------------
-- M2-script studying the components of the strata parametrising octic curves with at most two non-simple singularities which were not already handled in parameterFreeCases.m2. I.e., N_{2bar}, N_{1bar}, N_{12}, N_{12bar}, N_{1bar2}, N_{1bar2bar}, N_{22bar}, N_{2bar^2}, N_{11bar} and N_{1bar^2}.
--
-- Filename upToTwoSingularities.m2
--
-- Written by Ben Anthes
--
----------------------------------------------------------------------
--
clearAll();
load "octicsFunctions.m2";
--
-- Notation:  (will be extended later)
-- A0 is the affine coordinate ring of rational AA^3 with coordinates s_0, s_1 and t. 
-- S0 = A0[x,y,z] is the homogeneous coordinate ring of the trivial PP^2-bundle over AA^3.
--
-- We consider the points P_0 = (0;0;1) and P_1 = (1;0;0) and single out special lines {y = tx} and {z = 0} through these points. These distinguished lines are the vanishing loci of the linear forms L_0 and L_1, respectively.
--
-- Further below, we will consider various linear affine sub-spaces of AA^3 (t = 0 or 1, for example), with affine coordinate ring A and S = A[x,y,z] will be the homogeneous coordinate ring of the restricted PP^2-bundle. 
--
A0 = QQ[s_0,s_1,t];
S0 = A0[x,y,z];
L = {x,y};
T = {y-t*x,z};
F = {z,x};
P = apply(L,T,pair->trim ideal pair);
P33 = apply(2,i->get33Ideal(L_i,T_i,F_i));
Pd4 = apply(2,i->getDegenerateQuadrupleIdeal(L_i,T_i,F_i));
--
-- I.  Degenerate [3;3]-points
--
-- I.1  One degenerate [3;3]-point (N_{1bar})
-- We put a [3;3]-point at P_1 with distinguished tangent line {z = 0}
(RS,inc,params,f) = universalFamily(8,P33_1);
assert(#params == 33);
M1 = directedDegConditions33Point(f,inc L_1, inc T_1, inc F_1, s_1);
assert(numgens target M1 == 2 and isSurjective M1);
-- The kernel of M defines the linear system of octics with a degenerate [3;3]-point at P_1 with distinguished tangent line {z = 0} and second order direction parametrised by s_1, we conclude that this linear space has dimension 33 - 2 - 1 = 30; since the plane configuration of a point and a tangent direction at that point has stabiliser of dimension 5 (in PGL(3,CC)), we conclude that the stratum is of dimension 30 + 1 - 5 = 26.
-- It is easy to construct examples where s = 0 or s != 0 geometrically, so we omit the construction of an explicit inhabitant here.
--
-- I.2  A degenerate and a non-degenerate [3;3]-point (N_{11bar})
-- Recall that the distinguished tangents must not point towards the other [3;3]-point. Thus, we might as well choose t = 1.
A = A0/(t-1);
pr = map(A,A0);
S = A[gens S0];
prS = map(S,S0);
I33s = intersect apply(P33, i-> prS i);
(RS,inc,params,f) = universalFamily(8,I33s);
assert(#params == 21);
-- We impose degeneracy at P_1 as above:
M1 = directedDegConditions33Point(f,
    inc prS L_1, inc prS T_1, inc prS F_1, s_1);
assert(numgens target M1 == 2 and isSurjective M1);
-- Thus, the kernel has dimension 21 - 2 = 19 everywhere and so the stratum (non-empty by I.4 below) is irreducible of dimension 19 + 1 - 3 = 17 (+1 from s_1 and -3 from the stabiliser group).
--
-- I.3  Two degenerate [3;3]-points (N_{1bar^2})
-- Now we also let the [3;3]-point at P_0 degenerate:
M0 = directedDegConditions33Point(f,
    inc prS L_0, inc prS T_0, inc prS F_0, s_0);
N = M0 || M1;
assert(numgens target N == 4 and isSurjective N);
-- Since N is of maximal everywhere, the corresponding stratum  (inhabited by I.4 below) is of dimension 21 - 4 + 2 - 3 = 16; here 4 is the rank of N, +2 from s_0 and s_1 and -3 from the stabiliser group in GL(3,CC).
--
-- I.4  The strata are indeed inhabited
-- To show that N_{11bar} and N_{1bar^2} are actually inhabited, it is sufficient to construct octics in the family constrained by N of I.3 for any t != 0 and any s_i != 0. So that we can be sure that s_i = 0 does not cause problems, we also construct curves with this property:
S' = QQ[gens S];
special00 = map(S',S, gens(S') | {0,0,1});
special01 = map(S',S, gens(S') | {0,1,1});
special11 = map(S',S, gens(S') | {1,1,1});
-- The following four lines produce pseudo-random members which were hard-coded below.
--J = sub(idealFromKernel(N,f,params),S);
--fRand00 = randomElement(special00 J);
--fRand01 = randomElement(special01 J);
--fRand11 = randomElement(special11 J);
fRand00 = (3/4)*x*y^7-(45/64)*y^8+(5/18)*x^2*y^5*z+(9/8)*x*y^6*z-(3/10)*y^7*z+(2/9)*x^4*y^2*z^2+(3/2)*x^3*y^3*z^2-(1/2)*x^2*y^4*z^2+(28/9)*x*y^5*z^2-(13/3)*y^6*z^2+(5/3)*x^5*z^3+(10/3)*x^4*y*z^3-(1/2)*x^3*y^2*z^3+(12/5)*x^2*y^3*z^3-(749/30)*x*y^4*z^3+(271/15)*y^5*z^3-(9/80)*x^4*z^4-(5/28)*x^3*y*z^4-(9/8)*x^2*y^2*z^4+(453/140)*x*y^3*z^4-(1019/560)*y^4*z^4-x^3*z^5+3*x^2*y*z^5-3*x*y^2*z^5+y^3*z^5;
fRand01 = (188/105)*x^2*y^6-(7/4)*x*y^7+(36/25)*y^8-(102/35)*x^3*y^4*z-(1/10)*x^2*y^5*z+(1/70)*x*y^6*z-(10/27)*y^7*z+(16/35)*x^4*y^2*z^2-(2/3)*x^3*y^3*z^2-(7/54)*x^2*y^4*z^2+(1/2)*x*y^5*z^2-(152/945)*y^6*z^2+(2/3)*x^5*z^3+3*x^4*y*z^3-(2/15)*x^3*y^2*z^3-(35/18)*x^2*y^3*z^3-(497/45)*x*y^4*z^3+(851/90)*y^5*z^3-(5/4)*x^4*z^4-(9/35)*x^3*y*z^4-14*x^2*y^2*z^4+(1182/35)*x*y^3*z^4-(2557/140)*y^4*z^4-(14/15)*x^3*z^5+(14/5)*x^2*y*z^5-(14/5)*x*y^2*z^5+(14/15)*y^3*z^5;
fRand11 = (29/6)*x^2*y^6+(2/27)*x*y^7-(7/8)*y^8-(31/6)*x^3*y^4*z-(35/8)*x^2*y^5*z-(9/5)*x*y^6*z-(3/2)*y^7*z-(25/6)*x^4*y^2*z^2-(9/5)*x^3*y^3*z^2-5*x^2*y^4*z^2+(1/3)*x*y^5*z^2+(7937/210)*y^6*z^2+(9/2)*x^5*z^3-(14/3)*x^4*y*z^3+(4/7)*x^3*y^2*z^3-x^2*y^3*z^3+(10313/210)*x*y^4*z^3-(1698/35)*y^5*z^3+(10/7)*x^4*z^4+5*x^3*y*z^4+(9/35)*x^2*y^2*z^4-(743/35)*x*y^3*z^4+(509/35)*y^4*z^4-(5/3)*x^3*z^5+5*x^2*y*z^5-5*x*y^2*z^5+(5/3)*y^3*z^5;
-- First of all, the singular loci are supported at P_0 and P_1 only:
expected = sub(intersect(prS P_0, prS P_1),S');
sing00 = ideal jacobian matrix fRand00;
sing01 = ideal jacobian matrix fRand01;
sing11 = ideal jacobian matrix fRand11;
assert(radical sing00 == expected);
assert(radical sing01 == expected);
assert(radical sing11 == expected);
-- On the other hand, we have there the mildest possible degenerate [3;3]-points, namely, singularities of type J_{2,1}:
assert(isJ21(fRand00,special00 prS T_0,x,y,z));
assert(isJ21(fRand00,special00 prS T_1,y,z,x));
assert(isJ21(fRand01,special01 prS T_0,x,y,z));
assert(isJ21(fRand01,special01 prS T_1,y,z,x));
assert(isJ21(fRand11,special11 prS T_0,x,y,z));
assert(isJ21(fRand11,special11 prS T_1,y,z,x));
--
-- A suitable deformation will have the same singularity at P_0, but a non-degenerate [3;3]-point at P_1, say, so that we also conclude that the other stratum is non-empty.
--
-- II.  Degenerate quadruple-points
--
use S0;
(RS,inc,params,f) = universalFamily(8,P_1^4);
assert(#params == 35);
-- This is the universal family of octic forms whose associated curves have a quadruple-point at P_1.
--
-- II.1  A single degenerate quadruple-point (N_{2bar})
-- Imposing degeneracy:
M1 = tangencyCond(f,
    inc L_1, inc T_1, inc F_1, Multiplicity=>4, Degree=>2);
assert(numgens target M1 == 2 and isSurjective M1);
-- Hence, the moduli space of plane octics with a degenerate quadruple-point, which is clearly inhabited, is of dimension 35 - 2 - 6 = 27. (Here, 6 is the dimension of the stabiliser sub-group of GL(3,CC)).
--
-- II.2  Two quadruple-points, one degenerate (N_{22bar})
-- As before, we can take t = 1 since (on a reduced octic) there is no second quadruple-point on a special tangent line of a degenerate quadruple-point.
use S;
Iquads = intersect(prS P_0^4, prS P_1^4);
(RS,inc,params,f) = universalFamily(8, Iquads);
assert(#params == 25);
-- The degeneracy conditions are encoded in the vanishing of the following matrices:
M0 = tangencyCond(f,
    inc prS L_0, inc prS T_0, inc prS F_0,
    Multiplicity=>4, Degree=>2);
M1 = tangencyCond(f,
    inc prS L_1, inc prS T_1, inc prS F_1,
    Multiplicity=>4, Degree=>2);
assert(numgens target M0 == 2 and numgens target M1 == 2);
-- Imposing degeneracy at P_0 only:
assert isSurjective M0;
-- Thus, the rank of M0 is 2 and so the dimension of the corresponding stratum is 25 - 2 - 4 = 19; it is easy to see that it is actually inhabited.
--
-- II.3  Two quadruple-points, both degenerate (N_{2bar^2})
-- Imposing degeneracy at P_1 as well:
N = M0 || M1;
assert isSurjective N;
-- Thus, the rank of N is 4 and so the corresponding stratum is irreducible of dimension 25 - 4 - 3 = 18; again, it is easy to see that it is inhabited. For convenience, we construct a member:
use S';
-- The following two lines in comments were used to produce the hardcoded example fRand below.
--J = sub(idealFromKernel(N,f,params),S');
--fRand = randomElement(J);
fRand = -(3/8)*x^3*y^5-7*x^2*y^6+2*x*y^7-(15/7)*y^8-(10/9)*x^3*y^4*z+(49/20)*x^2*y^5*z-(3/5)*x*y^6*z+y^7*z+(35/4)*x^4*y^2*z^2-9*x^3*y^3*z^2+(35/27)*x^2*y^4*z^2-(18/7)*x*y^5*z^2-(7/9)*y^6*z^2+(7/4)*x^4*y*z^3-(10/9)*x^3*y^2*z^3-(1/4)*x^2*y^3*z^3+(18/5)*x*y^4*z^3-(15/16)*y^5*z^3+(3/50)*x^4*z^4-(10/3)*x^3*y*z^4+4*x^2*y^2*z^4+(44/25)*x*y^3*z^4-(373/150)*y^4*z^4;
-- First of all, its singular locus is supported at P_0 and P_1:
expected = sub(intersect(prS P_0, prS P_1),S');
sing = ideal jacobian matrix fRand;
assert(radical sing == expected);
-- On the other hand, at these points, we have the mildest possible degenerate quadruple-points singularities:
assert(isX10(fRand,x,y));
assert(isX10(fRand,z,y));
-- Thus, the curve defined by fRand has only two singular points, which are quadruple-points of type X_{10}.
--
--
-- III.  A quadruple- and a [3;3]-point
-- Putting the [3;3]-point at P_0 and the quadruple-point at P_1, we have to distinguish the two cases whether the distinguished tangent at P_0 points towards the quadruple-point (t = 0) or not.
--
-- III.1  The non-degenerate case (N_{12})
-- From parameterFreeCases.m2 III.1.1 we know that something special is happening if t = 0. Here we show that there are two disjoint components.
use A0;
-- The universal family with a [3;3]-point at P_0 with varying tagnent
(RS,inc,params,f) = universalFamily(8, P33_0);
assert(#params == 33);
-- Asking for a quadruple-point at P_1:
toBeZero = f%inc P_1^4;
toBeZeroCoeff = for Term in terms toBeZero list leadCoefficient(Term);
M = matrix for eq in toBeZeroCoeff list
               for g in params list leadCoefficient(eq//g);
-- M is a matrix A0^10 <-- A0^33. Its kernel encodes the parameters for which f has a quadruple-point at P_1. Thus, we have to see how the kernel changes as t varies. We investigate where the rank (generically 10) drops:
assert(numgens target M == 10);
droppingRankCond = minors(10, mingens image M);
assert(radical droppingRankCond == ideal(t));
-- Thus, the rank drops only if t = 0 and from parameterFreeCases.m2
-- III.1.1 we know that it drops exactly by 1 there.
--
-- For t != 0, this is the ideal containing the octic forms whose associated curves have a quadruple-point at P_1 and a [3;3]-point at P_0 with distinguished tangent line {y = tx}:
J = sub(idealFromKernel(M,f,params), S0);
-- Those with distinguished tangent line {y = 0} are obtained from the kernel if we substitute t -> 0 first:
Jspecial = sub(
    idealFromKernel(sub(M, {t=>0}), sub(f, {t=>0}), params), S0);
-- Substituting t -> 0 in J we get those which have distinguished tangent line {y = 0} and which are degenerations of such with t != 0:
Jintersection = sub(J,{t=>0});
-- Sanity check:
assert isSubset(Jintersection, Jspecial);
-- However:
assert not isSubset(Jspecial, Jintersection);
-- Thus, we get two distinct components. Their intersection consists of non-reduced curves only:
assert((gens Jintersection)%(sub(T_0,{t=>0}))^2 == 0);-- but
assert not ((gens Jspecial)%(sub(T_0,{t=>0}))^2 == 0);-- whereas
assert((gens Jspecial)%(sub(T_0,{t=>0})) == 0);
-- That is, every member of the component with t = 0 contains the line {y = 0} at least once, but not all do so twice, whereas those which are limits as t -> 0 contain this line at least twice. Since we neglect non-reduced curves in N_{12}, once we have shown -- that both are inhabited, we get two irreducible components N'_{12} and N''_{12}, both of dimension 19 as computed in III.1.1 of parameterFreeCases.m2. But since it is easy to construct examples by elementary plane curve-geometry, we omit this here.
--
-- III.2  A [3;3]-point and a quadruple-point, at least one degenerate
-- Note that III.1 shows that also in the degenerate cases, the two components distinguishing t != 0 and t = 0 are disjoint. Therefore, we don't need to let t vary but consider the cases t = 1 and t = 0.
--
-- First we let t = 1 (N'_{12bar}, N'_{1bar2}, N'_{1bar2bar}).
use S;
Imixed = intersect(prS P33_0, prS P_1^4);
(RS,inc,params,f) = universalFamily(8, Imixed);
assert(#params == 23);
-- The degeneracy conditions:
M0 = directedDegConditions33Point(f,
    inc prS L_0, inc prS T_0, inc prS F_0, s_0);
M1 = tangencyCond(f,
    inc prS L_1, inc prS T_1, inc prS F_1,
    Multiplicity=>4, Degree=>2);
assert(numgens target M0 == 2 and isSurjective M0);
assert(numgens target M1 == 2 and isSurjective M1);
-- Thus, the two components where either the [3;3]-point (N'_{1bar2}) or the quadruple-point (N'_{12bar}) is degenerate are irreducible and of dimension 23 - 2 + 1 - 4 = 23 - 2 - 3 = 18.
--
-- Both degenerate:
N = M0 || M1;
assert isSurjective N;
-- Thus, also the component N'{1bar2bar} is irreducible and of dimension 23 - 4 + 2 - 4 = 17. That these strata are inhabited is easy to see geometrically, so we omit constructions of explicit polynomials.
--
-- Now we let t = 0 (N''_{12bar}, N''_{1bar2}, N''_{1bar2bar}).
use A0;
B = A0/t;
prB = map(B,A0);
BS = B[gens S0];
prBS = map(BS,S0);
Imixed' = intersect(prBS P33_0, prBS P_1^4);
(RS,inc,params,f) = universalFamily(8, Imixed');
assert(#params == 24);
-- The degeneracy conditions:
M0 = directedDegConditions33Point(f,
    inc prBS L_0, inc prBS T_0, inc prBS F_0, s_0);
M1 = tangencyCond(f,
    inc prBS L_1, inc prBS T_1, inc prBS F_1,
    Multiplicity=>4, Degree=>2);
assert(numgens target M0 == 2 and numgens target M1 == 2);
-- Imposing degeneracy at the quadruple-point:
assert isSurjective M1;
-- Thus, the rank of M1 is constant and so the component N''_{12bar} is irreducible and of dimension 24 - 2 - 4 = 18.
-- Imposing degeneracy at the [3;3]-point:
droppingRankCond = minors(2,mingens image M0);
assert(radical droppingRankCond == ideal(s_0));
-- M0 is not surjective if and only if s_0 = 0. However, in this case, all octics will contain the distinguished tangent line twice:
Jspecial = sub(idealFromKernel(sub(M0,{s_0=>0}),sub(f,{s_0=>0}),params),BS);
assert((gens Jspecial)%(prBS T_0)^2 == 0);
-- Therefore, this case is irrelevant and for the remaining part, we see that we get an irreducible component N''_{1bar2} of dimension 24 - 2 + 1 - 5 = 18 as well.
--
-- Imposing degeneracy at both points:
N = M0 || M1;
droppingRankCond = minors(4,mingens image N);
assert(radical droppingRankCond == ideal(s_0));
-- The same as before is happening; again in this case all octics will be non-reduced if s_0 = 0, so that neglecting this part, we get an irreducible component N''_{1bar2bar} of dimension 24 - 4 + 2 - 5 = 17. Again, producing inhabitants is easy, hence omitted.
--
-- EOF upToTwoSingularities.m2 ---------------------------------------